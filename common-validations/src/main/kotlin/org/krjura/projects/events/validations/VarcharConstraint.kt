package org.krjura.projects.events.validations

import org.hibernate.validator.constraints.Length
import javax.validation.Constraint
import javax.validation.ReportAsSingleViolation
import javax.validation.constraints.NotEmpty
import kotlin.reflect.KClass

@MustBeDocumented
@Constraint(validatedBy = [])
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.FIELD)
@ReportAsSingleViolation
@NotEmpty
@Length(min = 1, max = 255)
annotation class VarcharConstraint (
    val message: String = "krjura.VarcharConstraint",
    val groups: Array<KClass<out Any>> = [],
    val payload: Array<KClass<out Any>> = []
)