package org.krjura.projects.events.validations

import javax.validation.Constraint
import javax.validation.ReportAsSingleViolation
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Pattern
import kotlin.annotation.MustBeDocumented
import kotlin.reflect.KClass

@MustBeDocumented
@Constraint(validatedBy = [])
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.FIELD)
@ReportAsSingleViolation
@NotEmpty
@Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}")
annotation class DateConstraint (
    val message: String = "krjura.DateConstraint",
    val groups: Array<KClass<out Any>> = [],
    val payload: Array<KClass<out Any>> = []
)