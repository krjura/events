package org.krjura.projects.flyway.events;

import org.flywaydb.core.Flyway;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class FlywayExecutor {

    private MigrationType type;

    private DatasourceConfiguration ds;

    public FlywayExecutor(MigrationType type, DatasourceConfiguration ds) {
        this.type = Objects.requireNonNull(type);
        this.ds = Objects.requireNonNull(ds);
    }

    public void execute() {
        Flyway flyway = Flyway
                .configure(FlywayExecutor.class.getClassLoader())
                .cleanDisabled(true)
                .encoding(StandardCharsets.UTF_8)
                .ignoreFutureMigrations(false)
                .ignorePendingMigrations(false)
                .outOfOrder(false)
                .locations(LocationResolver.resolve(this.type))
                .schemas("public")
                .dataSource(ds.getUrl(), ds.getUsername(), ds.getPassword())
                .load();

        flyway.migrate();


    }
}
