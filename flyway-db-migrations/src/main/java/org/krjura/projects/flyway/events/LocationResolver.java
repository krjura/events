package org.krjura.projects.flyway.events;

public final class LocationResolver {

    private LocationResolver() {
        // util
    }

    public static String resolve(MigrationType type) {
        switch (type) {
            case EVENTS:
                return "/migrations/events";
            default:
                throw new IllegalArgumentException("unknown type " + type);
        }
    }
}
