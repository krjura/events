package org.krjura.projects.flyway.events;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class FlywayMigrations {

    private static final Logger logger = Logger.getLogger(FlywayMigrations.class.getSimpleName());

    private static final String CONST_URL = "url";
    private static final String CONST_USERNAME = "username";
    private static final String CONST_PASSWORD = "password";

    public static void main(String[] args) {

        try {
            process(args);
        } catch (IllegalArgumentException e) {
            logger.warning("migration cannot be executed. The following error was returned: " + e.getMessage());
            help();
        }
    }

    private static void process(String[] args) {
        Map<String, String> params = extractParams(args);

        String url = requireParam(params, CONST_URL);
        String username = requireParam(params, CONST_USERNAME);
        String password = requireParam(params, CONST_PASSWORD);

        FlywayExecutor flywayExecutor = new FlywayExecutor(
                MigrationType.EVENTS,
                DatasourceConfiguration.newBuilder().withUrl(url).withUsername(username).withPassword(password).build()
        );

        flywayExecutor.execute();
    }

    private static String requireParam(Map<String, String> params, String paramName) {
        String paramValue = params.get(paramName);

        if(paramValue == null) {
            throw new IllegalArgumentException("value for param " + paramName + " not specified");
        }

        return paramValue;
    }

    private static Map<String, String> extractParams(String[] args) {
        Map<String, String> params = new HashMap<>();

        for(String arg : args) {
            String[] paramValues = arg.split("=");

            if(paramValues.length != 2) {
                throw new IllegalArgumentException("invalid param " + arg);
            }

            String key = fixKey(paramValues[0]);
            String value = paramValues[1];

            params.put(key, value);

            logger.info("processed param " + arg + " with key: " + key + " and value: " + value);
        }

        return params;
    }

    private static String fixKey(String key) {
        return key.replace("--", "");
    }

    private static void help() {
        String help =
                "\n" +
                "usage: --url=<url> --username=<username> --password=<password>" + "\n" +
                "--url to specify data source url" + "\n" +
                "--username to specify data source username" + "\n" +
                "--password to specify data source password" + "\n";

        logger.info(help);
    }
}
