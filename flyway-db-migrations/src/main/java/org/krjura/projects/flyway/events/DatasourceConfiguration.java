package org.krjura.projects.flyway.events;

public class DatasourceConfiguration {

    private final String url;

    private final String username;

    private final String password;

    private DatasourceConfiguration(Builder builder) {
        url = builder.url;
        username = builder.username;
        password = builder.password;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private String url;
        private String username;
        private String password;

        private Builder() {
        }

        public Builder withUrl(String val) {
            url = val;
            return this;
        }

        public Builder withUsername(String val) {
            username = val;
            return this;
        }

        public Builder withPassword(String val) {
            password = val;
            return this;
        }

        public DatasourceConfiguration build() {
            return new DatasourceConfiguration(this);
        }
    }
}
