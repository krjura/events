FROM docker.krjura.org/events/java-exec-env-jlink:1

LABEL maintainer="Krešimir Jurasović <krjura@protonmail.com>"

ENV APPLICATION_HOME=/opt/events \
    APPLICATION_USER=events \
    APPLICATION_GROUP=events

RUN useradd --create-home --uid 1200 $APPLICATION_USER \
    && mkdir -p $APPLICATION_HOME/logs \
    && mkdir -p $APPLICATION_HOME/tmp \
    && mkdir -p $APPLICATION_HOME/etc \
    && chown -R $APPLICATION_USER:$APPLICATION_GROUP $APPLICATION_HOME \
    && chmod -R a-w,o-rwx $APPLICATION_HOME \
    && chmod -R ug+w $APPLICATION_HOME/etc $APPLICATION_HOME/logs $APPLICATION_HOME/tmp

WORKDIR /opt/events
USER $APPLICATION_USER

COPY --chown=1200:1200 dist/services-events-core $APPLICATION_HOME

EXPOSE 25000

CMD java $JAVA_OPTS -jar events-boot.jar