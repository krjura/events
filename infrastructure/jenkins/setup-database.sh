#!/bin/bash

set -o xtrace

DIR=$( dirname "${BASH_SOURCE[0]}" )
echo "directory is $DIR"

# database settings
export PGHOST=database
export PGPORT=5432

export PGUSER=postgres
export PGPASSWORD=password

# wait until postgres server is up
for i in {1..10}
do
    if psql -c "select 1" > /dev/null
    then
        echo "Postgres is up"
    else
        echo "Postgres is down"
        sleep 1
    fi
done

# initialize postgres tablespace to use tmpfs
psql -f $DIR/init-tablespace.sql

# create events database
psql -f services-events-core/etc/init/db/create-databases-in-memory.sql