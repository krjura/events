#!/bin/bash

./gradlew --version
./gradlew --no-daemon --info -p flyway-db-migrations clean build bootJar publishAllPublicationsToMavenRepository