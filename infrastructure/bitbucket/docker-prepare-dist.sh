#!/bin/bash

DIST_FOLDER=dist/services-events-core

rm -rf $DIST_FOLDER
mkdir -p $DIST_FOLDER
mkdir -p $DIST_FOLDER/etc

cp -r services-events-core/build/libs/events-boot.jar $DIST_FOLDER/events-boot.jar
cp -r services-events-core/etc/config $DIST_FOLDER/etc/config
cp -r frontend-events/dist/frontend-events $DIST_FOLDER/web-resources

chmod -R a-w,o-rwx $DIST_FOLDER