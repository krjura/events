node {

  emailRecipients = 'krjura@gmail.com'

  try {

    build()

    notificationBuildSuccessful()

  } catch(err) {
    notificationBuildFailed()
    throw err
  }
}

def build() {

  resolveProperties()

  stage('Initialize') {
    checkout scm
  }

  stage('Build Frontend') {
    withCredentials([
      usernamePassword(credentialsId: 'MAVEN2_KRJURA_ORG_CREDENTIALS', usernameVariable: 'REPOSITORY_USERNAME', passwordVariable: 'REPOSITORY_PASSWORD')
    ]) {
      withEnv([
        'GRADLE_USER_HOME=/opt/cache/gradle',
        'ENVIRONMENT=jenkins']) {
        withDockerRegistry([credentialsId: 'DOCKER_KRJURA_ORG_CREDENTIALS', url: "https://docker.krjura.org"]) {

          stage('Build frontend') {
            docker.image('docker.krjura.org/events/angular-build-env:1').inside("-v events-npm-cache:/opt/node/npm-cache") {
              sh './frontend-events/build-frontend.sh'
            }
          }

          stage('Build backend') {
            docker.image('docker.krjura.org/postgres:11.4 ').withRun(" --mount type=tmpfs,destination=/opt/tmpfs ") { database ->
              docker.image('docker.krjura.org/events/java-build-env:1').inside("--link=${database.id}:database -v events-s-gradle-cache:${GRADLE_USER_HOME}") {
                sh './infrastructure/jenkins/setup-database.sh'
                
                sh './infrastructure/jenkins/build-services-events-core.sh'

                // do not publish flyway migrations if not on master
                if(env.BRANCH_NAME == "master") {
                  sh './infrastructure/jenkins/build-flyway-db-migrations.sh'
                }
              }
            }
          }

          if(env.BRANCH_NAME == "master") {
            stage('dockerize') {
              withDockerRegistry([credentialsId: 'DOCKER_KRJURA_ORG_CREDENTIALS', url: "https://docker.krjura.org"]) {

                IMAGE_TAG="docker.krjura.org/events/events-backend:${BUILD_NUMBER}"

                buildImage = docker.build("${IMAGE_TAG}", "${WORKSPACE}")
                buildImage.push();
                sh "docker rmi ${IMAGE_TAG}"
              }
            }
          }
        }
      }
    }
  }
}

def notificationBuildFailed() {
  if(env.BRANCH_NAME != "master") {
    return;
  }

  mail to: emailRecipients,
    subject: "Job '${JOB_NAME}' build ${BUILD_DISPLAY_NAME} has FAILED",
    body: "Please go to ${BUILD_URL} for details."
}

def notificationBuildSuccessful() {
  if(env.BRANCH_NAME != "master") {
    return;
  }

  if( currentBuild.previousBuild == null ) {
    return
  }

  if (currentBuild.previousBuild.result == 'FAILURE') {
    mail to: emailRecipients,
      subject: "Job '${JOB_NAME}' build ${BUILD_DISPLAY_NAME} has RECOVERED",
      body: "Please go to ${BUILD_URL} for details."
  }
}

def resolveProperties() {
  def config = []

  // make sure cleanup is done on a regular basis
  config.add(
    buildDiscarder(
      logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '30', numToKeepStr: '20')))

  config.add(disableConcurrentBuilds())

  properties(config)
}
