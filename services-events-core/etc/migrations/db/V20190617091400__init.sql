CREATE TABLE event_listing (
  id BIGSERIAL not null,

  user_ref VARCHAR(32) NOT NULL,

  begin_at date NOT NULL,
  occurs_every VARCHAR(16) NOT NULL,
  event_time time without time zone NOT NULL,
  next_occurrence timestamp with time zone NOT NULL,

  person_info VARCHAR(100) NOT NULL,
  note VARCHAR(200) NOT NULL,

  use_reminder BOOLEAN NOT NULL,

  CONSTRAINT pk_event_listing PRIMARY KEY (id)
);

CREATE TABLE event_listing_reminder (
  id BIGSERIAL not null,

  event_ref BIGINT NOT NULL,
  user_ref VARCHAR(32) NOT NULL,

  event_time time without time zone,
  occurs_before VARCHAR(16) NOT NULL,


  CONSTRAINT pk_event_listing_reminder PRIMARY KEY (id),
  CONSTRAINT fk_event_listing_reminder_event_ref FOREIGN KEY (event_ref) REFERENCES event_listing (id)
);