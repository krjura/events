CREATE TABLE notification_request (
  id BIGSERIAL NOT NULL,

  reminder_ref INTEGER NOT NULL,

  state VARCHAR(16) NOT NULL,
  type VARCHAR(16) NOT NULL,
  attempts INTEGER NOT NULL,
  execute_on TIMESTAMP WITHOUT TIME ZONE,

  CONSTRAINT pk_notification_request PRIMARY KEY (id)
);