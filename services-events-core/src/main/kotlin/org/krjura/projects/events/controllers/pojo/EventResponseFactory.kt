package org.krjura.projects.events.controllers.pojo

import org.krjura.projects.events.model.Event
import java.time.format.DateTimeFormatter

object EventResponseFactory {

    fun of(event: Event): EventResponse {
        return EventResponse(
                id = event.id!!,
                userId = event.userId,
                beginAt = event.beginAt.format(DateTimeFormatter.ISO_DATE),
                occursEvery = event.occursEvery,
                eventTime = event.eventTime.format(DateTimeFormatter.ISO_TIME),
                nextOccurrence = event.nextOccurrence.format(DateTimeFormatter.ISO_DATE_TIME),
                personInfo = event.personInfo,
                note = event.note,
                useReminder = event.useReminder
        )
    }
}