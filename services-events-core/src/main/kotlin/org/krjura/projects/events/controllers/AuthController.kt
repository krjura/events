package org.krjura.projects.events.controllers

import org.krjura.projects.events.controllers.pojo.AuthResponse
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class AuthController {

    @GetMapping(path = ["/api/v1/auth/config"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun config(): ResponseEntity<AuthResponse> {

        val authentication = SecurityContextHolder.getContext().authentication

        if(authentication is OAuth2AuthenticationToken) {
            val username = authentication.principal.attributes["email"] as String
            val privileges = authentication.authorities.map { it.authority }

            return ResponseEntity.ok(AuthResponse(authenticated = true, username = username, privileges = privileges))
        }

        return ResponseEntity.ok(AuthResponse(authenticated = false, username = null, privileges = emptyList()))
    }
}