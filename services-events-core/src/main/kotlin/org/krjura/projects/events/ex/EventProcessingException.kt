package org.krjura.projects.events.ex

import java.lang.RuntimeException

class EventProcessingException : RuntimeException {

    constructor(message: String?) : super(message)

    constructor(message: String?, cause: Throwable?) : super(message, cause)
}