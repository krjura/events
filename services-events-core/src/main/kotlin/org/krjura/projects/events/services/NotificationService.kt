package org.krjura.projects.events.services

import org.krjura.projects.events.enums.NotificationRequestState
import org.krjura.projects.events.enums.NotificationType
import org.krjura.projects.events.events.EventListener
import org.krjura.projects.events.events.EventChanged
import org.krjura.projects.events.events.ReminderCreated
import org.krjura.projects.events.events.ReminderDeleted
import org.krjura.projects.events.events.ReminderEventFactory
import org.krjura.projects.events.model.Event
import org.krjura.projects.events.model.Reminder
import org.krjura.projects.events.model.NotificationRequest
import org.krjura.projects.events.pojo.OccursParser
import org.krjura.projects.events.repository.ReminderRepository
import org.krjura.projects.events.repository.EventRepository
import org.krjura.projects.events.repository.NotificationRequestRepository
import org.krjura.projects.events.services.pojo.utils.NotificationCalculator
import org.krjura.projects.events.services.pojo.utils.OccurrenceCalculator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class NotificationService(
        private val eventRepository: EventRepository,
        private val reminderRepository: ReminderRepository,
        private val notificationRepository: NotificationRequestRepository): EventListener {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(NotificationService::class.java)
    }

    override fun supports(): List<Class<*>> {
        return listOf(
                ReminderCreated::class.java,
                ReminderDeleted::class.java,
                EventChanged::class.java
        )
    }

    override fun onEvent(event: Any) {
        when (event) {
            is ReminderCreated -> processReminderChanged(event)
            is ReminderDeleted -> processReminderDeleted(event)
            is EventChanged -> processEventUpdated(event)
        }
    }

    private fun processEventUpdated(event: EventChanged) {
        this
                .reminderRepository
                .findReminderByEventIdAndUserId(event.userId, event.eventId)
                .forEach { processReminderChanged(ReminderEventFactory.created(it)) }
    }

    private fun processReminderDeleted(event: ReminderDeleted) {
        this.notificationRepository.deleteByReminderId(event.reminderId)
    }

    private fun processReminderChanged(event: ReminderCreated) {
        logger.info("setting up notification for event ${event.eventId} and reminder ${event.reminderId}")

        val eventDataOptional = this.eventRepository.findByUserIdAndId(event.userId, event.eventId)
        val reminderDataOptional = this.reminderRepository.findByUserIdAndId(event.userId, event.reminderId)
        val notificationDataOptional = this.notificationRepository.findByReminderId(event.reminderId)

        // sanity check. Ensure event and reminder always exists
        if(eventDataOptional.isEmpty || reminderDataOptional.isEmpty) {
            return
        }

        val eventData = eventDataOptional.get()
        val reminderData = reminderDataOptional.get()

        // overwrite existing notification if it exists
        val requestId = if (notificationDataOptional.isPresent) notificationDataOptional.get().id else null
        val executeOn = calculateExecuteOn(eventData, reminderData)

        // if the reminder is in the past then there is no point in making this notification
        if(executeOn.isBefore(LocalDateTime.now())) {
            return
        }

        val request = NotificationRequest(
                id = requestId,
                reminderId = reminderData.id!!,
                type = NotificationType.REMINDER,
                state = NotificationRequestState.OPEN,
                attempts = 0,
                executeOn = executeOn
        )

        this.notificationRepository.save(request)
    }

    private fun calculateExecuteOn(eventData: Event, reminderData: Reminder): LocalDateTime {
        val eventOccurs = OccursParser.parse(eventData.occursEvery)
        val eventTime = OccurrenceCalculator.default.calculate(eventData.beginAt, eventData.eventTime, eventOccurs)

        return NotificationCalculator.calculate(eventTime, reminderData.occursBefore)
    }
}