package org.krjura.projects.events.services.pojo.utils

import org.krjura.projects.events.pojo.Occurs
import org.krjura.projects.events.pojo.OccursTimeUnit
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.temporal.ChronoUnit

class OccurrenceCalculator(private val timeSource: OccurrenceTimeProvider) {

    companion object {
        val default = OccurrenceCalculator(CurrentTimeProvider())
    }

    fun calculate(beginAt: LocalDate, eventTime: LocalTime, occursEvery: Occurs): LocalDateTime {
        return when(occursEvery.type) {
            OccursTimeUnit.DAY -> calculateWhenDay(beginAt, eventTime, occursEvery)
            OccursTimeUnit.WEEK -> calculateWhenWeek(beginAt, eventTime, occursEvery)
            OccursTimeUnit.MONTH -> calculateWhenMonth(beginAt, eventTime, occursEvery)
            OccursTimeUnit.YEAR -> calculateWhenYear(beginAt, eventTime, occursEvery)
        }
    }

    private fun calculateWhenYear(beginAt: LocalDate, eventTime: LocalTime, occursEvery: Occurs): LocalDateTime {
        val original = beginAt.atTime(eventTime)
        val current = timeSource.fetch()

        if(original.isAfter(current) || original.isEqual(current)) {
            return original
        }

        val yearsBetween = ChronoUnit.YEARS.between(original, current)
        val times = Math.floorDiv(yearsBetween, occursEvery.duration)

        val plusYears = occursEvery.duration + times * occursEvery.duration
        return original.plusYears(plusYears)
    }

    private fun calculateWhenMonth(beginAt: LocalDate, eventTime: LocalTime, occursEvery: Occurs): LocalDateTime {
        val original = beginAt.atTime(eventTime)
        val current = timeSource.fetch()

        if(original.isAfter(current) || original.isEqual(current)) {
            return original
        }

        val yearsBetween = ChronoUnit.MONTHS.between(original, current)
        val times = Math.floorDiv(yearsBetween, occursEvery.duration)

        val plusMonths = occursEvery.duration + times * occursEvery.duration
        return original.plusMonths(plusMonths)
    }

    private fun calculateWhenWeek(beginAt: LocalDate, eventTime: LocalTime, occursEvery: Occurs): LocalDateTime {
        val original = beginAt.atTime(eventTime)
        val current = timeSource.fetch()

        if(original.isAfter(current) || original.isEqual(current)) {
            return original
        }

        val yearsBetween = ChronoUnit.WEEKS.between(original, current)
        val times = Math.floorDiv(yearsBetween, occursEvery.duration)

        val plusWeeks = occursEvery.duration + times * occursEvery.duration
        return original.plusWeeks(plusWeeks)
    }

    private fun calculateWhenDay(beginAt: LocalDate, eventTime: LocalTime, occursEvery: Occurs): LocalDateTime {
        val original = beginAt.atTime(eventTime)
        val current = timeSource.fetch()

        if(original.isAfter(current) || original.isEqual(current)) {
            return original
        }

        val yearsBetween = ChronoUnit.DAYS.between(original, current)
        val times = Math.floorDiv(yearsBetween, occursEvery.duration)

        val plusDays = occursEvery.duration + times * occursEvery.duration
        return original.plusDays(plusDays)
    }
}