package org.krjura.projects.events.services.pojo.utils

import java.time.LocalDateTime

interface OccurrenceTimeProvider {

    fun fetch(): LocalDateTime
}

class CurrentTimeProvider: OccurrenceTimeProvider {

    override fun fetch(): LocalDateTime {
        return LocalDateTime.now()
    }
}

class SpecificTimeProvider(private val ldt: LocalDateTime): OccurrenceTimeProvider {

    override fun fetch(): LocalDateTime {
        return this.ldt
    }
}