package org.krjura.projects.events.controllers.graphql.context

import org.krjura.projects.events.model.Reminder

data class EventListingContext(val loaded: Boolean, val reminders: List<Reminder> = emptyList())