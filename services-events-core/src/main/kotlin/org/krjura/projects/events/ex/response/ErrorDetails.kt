package org.krjura.projects.events.ex.response

data class ErrorDetails(
        val key: String,
        val message: String,
        val attributeName: String,
        val attributeValues: List<String>
)