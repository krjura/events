package org.krjura.projects.events.controllers

import org.krjura.projects.events.controllers.pojo.CreateEventReminderRequest
import org.krjura.projects.events.pojo.OccursParser
import org.krjura.projects.events.services.EventService
import org.krjura.projects.events.services.pojo.CreateEventReminder
import org.krjura.projects.events.utils.AuthUtils
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import javax.validation.Valid

@RestController
data class EventReminderController(private val eventService: EventService) {

    @PostMapping(
            path = ["/api/v1/events/{eventId}/reminders"],
            consumes = [MediaType.APPLICATION_JSON_VALUE],
            produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun createEventReminder(
            @PathVariable("eventId") eventId: Long,
            @Valid @RequestBody request: CreateEventReminderRequest): ResponseEntity<Void> {

        AuthUtils.checkIsUser()

        val userInfo = AuthUtils.extractUserInfo();

        val createRequest = CreateEventReminder(
                userId = userInfo.email,
                eventId = eventId,
                eventTime = LocalTime.parse(request.eventTime, DateTimeFormatter.ISO_TIME),
                occursBefore = OccursParser.parse(request.occursBefore)
        )

        this.eventService.createEventReminder(createRequest)

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path = ["/api/v1/events/{eventId}/reminders/{reminderId}"])
    fun deleteEventReminders(
            @PathVariable("eventId") eventId: Long, @PathVariable("reminderId") reminderId: Long): ResponseEntity<Void> {

        AuthUtils.checkIsUser()

        val userInfo = AuthUtils.extractUserInfo();

        this.eventService.deleteReminderByIdAndUserId(userInfo.email, eventId, reminderId)

        return ResponseEntity.noContent().build()
    }
}