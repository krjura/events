package org.krjura.projects.events.services.pojo

import org.krjura.projects.events.pojo.Occurs
import java.time.LocalDate
import java.time.LocalTime

data class CreateEvent(
        val beginAt: LocalDate,
        val occursEvery: Occurs,
        val eventTime: LocalTime,
        val personInfo: String,
        val note: String,
        val useReminder: Boolean
)