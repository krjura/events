package org.krjura.projects.events.config

import org.krjura.projects.events.enums.Privileges
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint

@Configuration
@EnableWebSecurity
open class WebSecurityConfig(private val env: Environment): WebSecurityConfigurerAdapter() {

    @Autowired
    @Throws(Exception::class)
    fun configureGlobal(auth: AuthenticationManagerBuilder) {
        val builder = auth.inMemoryAuthentication()

        if(isMonitoringEnabled()) {
            builder
                    .withUser(monitoringUsername())
                    .password(passwordEncoder().encode(monitoringPassword()))
                    .authorities(Privileges.ROLE_MONITORING.name)
        }
    }

    @Bean
    open fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder(10)
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
                .authorizeRequests()
                .antMatchers("/actuator/health").permitAll()
                .antMatchers("/actuator", "/actuator/**").hasAuthority(Privileges.ROLE_MONITORING.name)
                .antMatchers("/", "/portal", "/portal/**").permitAll()
                .antMatchers("/api/v1/auth/config").permitAll()
                .antMatchers("/**").authenticated()

        http.csrf().disable()

        if(isOauthEnabled()) {
            http.oauth2Login()
        }

        if(isMonitoringEnabled()) {
            http
                    .httpBasic()
                    .authenticationEntryPoint(Http403ForbiddenEntryPoint())
        }

        http
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler(logoutHandler());
    }

    private fun isOauthEnabled(): Boolean {
        return this.env.getProperty("krjura.security.oauth.enabled", Boolean::class.java, false)
    }

    private fun isMonitoringEnabled(): Boolean {
        return this.env.getProperty("krjura.events.monitoring.enabled", Boolean::class.java, false)
    }

    private fun monitoringUsername(): String {
        return this.env.getRequiredProperty("krjura.events.monitoring.username", String::class.java)
    }

    private fun monitoringPassword(): String {
        return this.env.getRequiredProperty("krjura.events.monitoring.password", String::class.java)
    }

    private fun logoutHandler(): SimpleUrlLogoutSuccessHandler {
        val handler = SimpleUrlLogoutSuccessHandler();
        handler.setDefaultTargetUrl("/portal")
        return handler;
    }
}