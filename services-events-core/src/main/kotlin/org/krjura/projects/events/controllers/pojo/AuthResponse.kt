package org.krjura.projects.events.controllers.pojo

data class AuthResponse(
        val authenticated: Boolean,
        val username: String?,
        val privileges: List<String>
)