package org.krjura.projects.events.repository

import org.krjura.projects.events.model.Event
import org.springframework.data.jdbc.repository.query.Modifying
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.Optional

interface EventRepository: CrudRepository<Event, Long> {

    @Query("SELECT e.* FROM event e")
    override fun findAll(): List<Event>

    @Query("SELECT e.* FROM event e WHERE e.user_ref = :userId AND id = :id")
    fun findByUserIdAndId(@Param("userId") userId: String, @Param("id") id: Long): Optional<Event>

    @Query("SELECT e.* FROM event e WHERE e.user_ref = :userId ORDER BY next_occurrence ASC LIMIT :limit OFFSET :offset")
    fun findAllByUserId(
            @Param("userId") userId: String,
            @Param("limit") limit: Int,
            @Param("offset") offset: Long
    ): List<Event>

    @Query("SELECT e.* FROM event e " +
            "WHERE e.user_ref = :userId AND e.person_info LIKE '%' || :searchText || '%' " +
            "ORDER BY next_occurrence ASC LIMIT :limit OFFSET :offset")
    fun findAllEventsByUserIdAndSearchText(
            @Param("userId") userId: String,
            @Param("searchText") searchText: String,
            @Param("limit") limit: Int,
            @Param("offset") offset: Long
    ): List<Event>

    @Query("SELECT e.* FROM event e WHERE e.next_occurrence < :to ORDER BY e.next_occurrence ASC LIMIT :limit")
    fun findExpired(@Param("to") to: LocalDateTime, @Param("limit") limit: Int): List<Event>

    @Query("UPDATE event " +
            "SET begin_at = :beginAt, occurs_every = :occursEvery, event_time = :eventTime, " +
            "next_occurrence = :nextOccurrence, " +
            "person_info = :personInfo, note = :note, use_reminder = :useReminder " +
            "WHERE user_ref = :userId AND id = :eventId"
    )
    @Modifying
    fun update(
            @Param("beginAt") beginAt: LocalDate,
            @Param("occursEvery") occursEvery: String,
            @Param("eventTime") eventTime: LocalTime,
            @Param("nextOccurrence") nextOccurrence: LocalDateTime,
            @Param("personInfo") personInfo: String,
            @Param("note") note: String,
            @Param("useReminder") useReminder: Boolean,
            @Param("userId") userId: String,
            @Param("eventId") eventId: Long
    )

    @Modifying
    @Query("UPDATE event SET next_occurrence = :nextOccurrence WHERE id = :id")
    fun updateNextOccurrence(@Param("id") id: Long, @Param("nextOccurrence") nextOccurrence: LocalDateTime)

    @Query("DELETE FROM event e WHERE e.user_ref = :userId AND e.id = :eventId")
    @Modifying
    fun deleteEventById(@Param("userId") userId: String, @Param("eventId") eventId: Long)
}