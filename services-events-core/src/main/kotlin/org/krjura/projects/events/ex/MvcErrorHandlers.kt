package org.krjura.projects.events.ex

import org.krjura.projects.events.ex.response.ErrorDetails
import org.krjura.projects.events.ex.response.ErrorResponse
import org.springframework.http.HttpStatus
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import java.util.ArrayList

@ControllerAdvice
class MvcErrorHandlers {

    companion object {
        const val STATUS_VALIDATION_ERROR_OCCURRED = "validation error occurred"
        const val STATUS_PROCESSING_ERROR_OCCURRED_KEY = "krjura.ex.EventProcessingException"
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(EventProcessingException::class)
    @ResponseBody
    fun handleEventProcessingException(ex: EventProcessingException): ErrorResponse {

        val details = mutableListOf(
                ErrorDetails(
                        key = STATUS_PROCESSING_ERROR_OCCURRED_KEY,
                        message = STATUS_PROCESSING_ERROR_OCCURRED_KEY,
                        attributeName = "",
                        attributeValues = emptyList()
                )
        )

        return ErrorResponse(ex.message!!, details)
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException::class)
    @ResponseBody
    fun handleMethodArgumentNotValidException(ex: MethodArgumentNotValidException): ErrorResponse {

        val fieldErrors = ArrayList(ex.bindingResult.fieldErrors)
        fieldErrors.sortBy { it.field }

        val details = mutableListOf<ErrorDetails>()

        for (fieldError in fieldErrors) {

            details.add(
                    ErrorDetails(
                            key = fieldError.defaultMessage!!,
                            message = fieldError.defaultMessage!!,
                            attributeName = fieldError.field,
                            attributeValues = listOf(getObjectValue(fieldError.rejectedValue!!))
                    )
            )
        }

        return ErrorResponse(STATUS_VALIDATION_ERROR_OCCURRED, details)
    }

    private fun getObjectValue(value: Any): String {
        return value.toString()
    }
}