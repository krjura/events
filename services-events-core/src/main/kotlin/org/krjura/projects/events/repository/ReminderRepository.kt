package org.krjura.projects.events.repository

import org.krjura.projects.events.model.Reminder
import org.springframework.data.jdbc.repository.query.Modifying
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import java.util.Optional

interface ReminderRepository : CrudRepository<Reminder, Long> {

    @Query("SELECT e.* FROM reminder e")
    override fun findAll(): List<Reminder>

    @Query("SELECT e.* FROM reminder e WHERE e.user_ref = :userId and e.event_ref = :eventId ORDER BY id DESC")
    fun findReminderByEventIdAndUserId(
            @Param("userId") userId: String, @Param("eventId") eventId: Long): List<Reminder>

    @Query("SELECT e.* FROM reminder e WHERE e.user_ref = :userId and e.event_ref in ( :eventIds ) ORDER BY id DESC")
    fun findRemindersByEventIdsAndUserId(@Param("userId") userId: String, @Param("eventIds") ids: List<Long>): List<Reminder>

    @Modifying
    @Query("DELETE FROM reminder e  WHERE e.user_ref = :userId and e.id = :reminderId")
    fun deleteReminderByIdAndUserId(@Param("userId") userId: String, @Param("reminderId") reminderId: Long)

    @Modifying
    @Query("DELETE FROM reminder e  WHERE e.user_ref = :userId and e.event_ref = :eventId")
    fun deleteRemindersByUserIdAndEventId(@Param("userId") userId: String, @Param("eventId") eventId: Long)

    @Query("SELECT e.* FROM reminder e WHERE e.user_ref = :userId and e.id = :id")
    fun findByUserIdAndId(@Param("userId") userId: String, @Param("id") reminderId: Long): Optional<Reminder>
}