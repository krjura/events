package org.krjura.projects.events.configprops

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "krjura.events.monitoring")
open class MonitoringProps(
    var enabled: Boolean = false,
    var username: String? = null,
    var password: String? = null
)