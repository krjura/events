package org.krjura.projects.events.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalTime

@Table("reminder")
data class Reminder (
        @Id val id: Long?,
        @Column("event_ref") val eventId: Long,
        @Column("user_ref") val userId: String,
        @Column("event_time") val eventTime: LocalTime,
        @Column("occurs_before") val occursBefore: String
)