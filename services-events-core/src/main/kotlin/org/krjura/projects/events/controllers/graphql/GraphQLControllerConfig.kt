package org.krjura.projects.events.controllers.graphql

import graphql.GraphQL
import graphql.schema.GraphQLSchema
import graphql.schema.idl.RuntimeWiring
import graphql.schema.idl.SchemaGenerator
import graphql.schema.idl.SchemaParser
import graphql.schema.idl.TypeRuntimeWiring.newTypeWiring
import org.krjura.projects.events.controllers.graphql.GraphQlDataFetchers
import org.krjura.projects.events.services.EventService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths

@Configuration
open class GraphQLControllerConfig {

    @Bean
    open fun graphQl(dataFetchers: GraphQlDataFetchers): GraphQL {

        val path = Paths.get("etc/config/schema.graphqls")
        val schemaAsString = String(Files.readAllBytes(path), StandardCharsets.UTF_8)

        val graphQLSchema = buildSchema(dataFetchers, schemaAsString)
        return GraphQL.newGraphQL(graphQLSchema).build()
    }

    @Bean
    open fun graphQlDataFetcher(eventService: EventService): GraphQlDataFetchers {
        return GraphQlDataFetchers(eventService)
    }

    private fun buildSchema(dataFetchers: GraphQlDataFetchers, sdl: String): GraphQLSchema {
        val typeRegistry = SchemaParser().parse(sdl)
        val runtimeWiring = buildWiring(dataFetchers)
        val schemaGenerator = SchemaGenerator()

        return schemaGenerator.makeExecutableSchema(typeRegistry, runtimeWiring)
    }

    private fun buildWiring(dataFetchers: GraphQlDataFetchers): RuntimeWiring {
        return RuntimeWiring
                .newRuntimeWiring()
                .type(newTypeWiring("Query").dataFetcher("eventsByUser", dataFetchers.eventsByUserFetcher()))
                .type(newTypeWiring("Query").dataFetcher("eventsById", dataFetchers.eventById()))
                .type(newTypeWiring("EventListing").dataFetcher("reminders", dataFetchers.remindersByEventId()))
                .build()
    }
}