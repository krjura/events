package org.krjura.projects.events.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@Table("event")
data class Event(
        @Id val id: Long?,
        @Column("user_ref") val userId: String,
        @Column("begin_at") val beginAt: LocalDate,
        @Column("occurs_every") val occursEvery: String,
        @Column("event_time") val eventTime: LocalTime,
        @Column("next_occurrence") val nextOccurrence: LocalDateTime,
        @Column("person_info") val personInfo: String,
        @Column("note") val note: String,
        @Column("use_reminder") val useReminder: Boolean
)