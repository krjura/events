package org.krjura.projects.events.configprops

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "krjura.events.scheduling")
open class SchedulingProps(
    var eventReschedulingEnabled: Boolean = false,
    var eventReschedulingFixedRate: Long? = null,
    var eventReschedulingDelay: Long? = null
)