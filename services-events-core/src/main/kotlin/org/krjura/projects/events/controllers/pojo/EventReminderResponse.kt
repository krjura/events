package org.krjura.projects.events.controllers.pojo

data class EventReminderResponse(
        val id: Long,
        val eventId: Long,
        val userId: String,
        val eventTime: String,
        val occursBefore: String
)
