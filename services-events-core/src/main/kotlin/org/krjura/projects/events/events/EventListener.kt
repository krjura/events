package org.krjura.projects.events.events

interface EventListener {

    fun supports(): List<Class<*>>

    fun onEvent(event: Any)
}