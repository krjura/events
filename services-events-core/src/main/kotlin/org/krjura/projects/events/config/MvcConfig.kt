package org.krjura.projects.events.config

import org.springframework.context.annotation.Configuration
import org.springframework.http.CacheControl
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import java.util.concurrent.TimeUnit

@Configuration
@EnableWebMvc
open class MvcConfig : WebMvcConfigurer {

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        registry
                .addResourceHandler("/portal/*.html", "/portal/*.txt")
                .addResourceLocations("file:web-resources/")
                .setCacheControl(CacheControl.noCache())

        registry
                .addResourceHandler("/portal/**")
                .addResourceLocations("file:web-resources/")
                .setCacheControl(CacheControl.maxAge(1, TimeUnit.DAYS))
    }
}