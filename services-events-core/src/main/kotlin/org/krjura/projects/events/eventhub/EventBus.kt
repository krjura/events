package org.krjura.projects.events.eventhub

import org.krjura.projects.events.events.EventListener
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class EventBus(val listeners: List<EventListener>) {

    private var listenerMap = HashMap<String, List<EventListener>>()

    @PostConstruct
    fun init() {
        for(listener in listeners) {
            for(clazz in listener.supports()) {
                val className = clazz.simpleName;
                this.listenerMap.merge(className, listOf(listener)) { current, additional -> current + additional}
            }
        }
    }

    fun send(event: Any) {
        val className = event.javaClass.simpleName;

        val receivers = this.listenerMap.getOrDefault(className, listOf())
        processListeners(receivers, event)
    }

    private fun processListeners(registeredListeners: List<EventListener>, event: Any) {
        for (registeredListener in registeredListeners) {
            registeredListener.onEvent(event)
        }
    }
}