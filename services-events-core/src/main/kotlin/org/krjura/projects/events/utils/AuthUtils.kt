package org.krjura.projects.events.utils

import org.krjura.projects.events.enums.Privileges
import org.krjura.projects.events.ex.EventProcessingException
import org.krjura.projects.events.pojo.UserInfo
import org.springframework.security.authentication.InsufficientAuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.core.user.OAuth2User

object AuthUtils {

    fun extractUserInfo(): UserInfo {
        val authentication = SecurityContextHolder.getContext().authentication
                ?: throw EventProcessingException("User is not logged in")

        val principal = authentication.principal;

        if(principal is OAuth2User) {

            val email = principal.attributes["email"] as String
            val privileges = authentication.authorities.map { it.authority }

            return UserInfo(email, privileges)
        }

        throw InsufficientAuthenticationException("User is not logged in")
    }

    fun checkIsUser() {
        val authentication = SecurityContextHolder.getContext().authentication
                ?: throw InsufficientAuthenticationException("User is not logged in")

        val authorities = authentication.authorities.map { it.authority }

        if(!authorities.contains(Privileges.ROLE_USER.name)) {
            throw InsufficientAuthenticationException("User does not have ROLE_USER privilege")
        }
    }
}