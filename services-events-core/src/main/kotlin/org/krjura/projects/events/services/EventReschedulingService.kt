package org.krjura.projects.events.services

import org.krjura.projects.events.configprops.ApplicationProps
import org.krjura.projects.events.eventhub.EventBus
import org.krjura.projects.events.events.EventChanged
import org.krjura.projects.events.model.Event
import org.krjura.projects.events.pojo.OccursParser
import org.krjura.projects.events.repository.EventRepository
import org.krjura.projects.events.services.pojo.utils.OccurrenceCalculator
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

@Service
class EventReschedulingService(
        private val applicationProps: ApplicationProps,
        private val eventRepository: EventRepository,
        private val eventBus: EventBus) {

    companion object {
        private var logger = LoggerFactory.getLogger(EventReschedulingService::class.java)
    }

    @Scheduled(
            fixedRateString = "\${krjura.events.scheduling.eventReschedulingFixedRate}",
            initialDelayString = "\${krjura.events.scheduling.eventReschedulingDelay}"
    )
    fun check() {
        if(!this.applicationProps.scheduling.eventReschedulingEnabled) {
            return
        }

        try {
            checkSafely();
        } catch (e: Exception ) {
            logger.warn("Cannot execute check", e)
        }
    }

    fun checkSafely() {
        val to = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS);
        this.eventRepository.findExpired(to, 10).forEach(this::process)
    }

    private fun process(event: Event) {
        logger.info("rescheduling event with id ${event.id}")

        val nextOccurrence: LocalDateTime = OccurrenceCalculator.default
                .calculate(event.beginAt, event.eventTime, OccursParser.parse(event.occursEvery))

        this.eventRepository.updateNextOccurrence(event.id!!, nextOccurrence)
        this.eventBus.send(EventChanged(event.userId, event.id))
    }
}