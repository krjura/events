package org.krjura.projects.events.events

import org.krjura.projects.events.model.Reminder

object ReminderEventFactory {

    fun created(reminder: Reminder): ReminderCreated {
        return ReminderCreated(
                userId = reminder.userId,
                eventId = reminder.eventId,
                reminderId = reminder.id!!
        )
    }

    fun deleted(reminder: Reminder): ReminderDeleted {
        return ReminderDeleted(
                userId = reminder.userId,
                eventId = reminder.eventId,
                reminderId = reminder.id!!
        )
    }
}