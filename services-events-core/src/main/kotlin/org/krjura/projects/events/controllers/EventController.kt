package org.krjura.projects.events.controllers

import org.krjura.projects.events.controllers.pojo.CreateEventRequest
import org.krjura.projects.events.pojo.OccursParser
import org.krjura.projects.events.services.EventService
import org.krjura.projects.events.services.pojo.CreateEvent
import org.krjura.projects.events.utils.AuthUtils
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import javax.validation.Valid

@RestController
class EventController(private var eventService: EventService) {

    @PostMapping(
            path = ["/api/v1/events"],
            consumes = [MediaType.APPLICATION_JSON_VALUE],
            produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun createEvent(@Valid @RequestBody request: CreateEventRequest): ResponseEntity<Void> {
        AuthUtils.checkIsUser()

        val userInfo = AuthUtils.extractUserInfo()

        val createEvent = CreateEvent(
                beginAt = LocalDate.parse(request.beginAt, DateTimeFormatter.ISO_DATE),
                occursEvery = OccursParser.parse(request.occursEvery),
                eventTime = LocalTime.parse(request.eventTime, DateTimeFormatter.ISO_TIME),
                personInfo = request.personInfo,
                note = request.note,
                useReminder = request.useReminder
        )

        eventService.createEvent(userInfo, createEvent)

        return ResponseEntity.noContent().build()
    }

    @PutMapping(
            path = ["/api/v1/events/{eventId}"],
            consumes = [MediaType.APPLICATION_JSON_VALUE],
            produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun updateEvent(
            @PathVariable("eventId") eventId: Long,
            @Valid @RequestBody request: CreateEventRequest): ResponseEntity<Void> {

        AuthUtils.checkIsUser()

        val userInfo = AuthUtils.extractUserInfo()

        val createEvent = CreateEvent(
                beginAt = LocalDate.parse(request.beginAt, DateTimeFormatter.ISO_DATE),
                occursEvery = OccursParser.parse(request.occursEvery),
                eventTime = LocalTime.parse(request.eventTime, DateTimeFormatter.ISO_TIME),
                personInfo = request.personInfo,
                note = request.note,
                useReminder = request.useReminder
        )

        this.eventService.updateEvent(userInfo, eventId, createEvent);

        return ResponseEntity.noContent().build()
    }

    @DeleteMapping(path = ["/api/v1/events/{eventId}"])
    fun deleteEventById(@PathVariable("eventId") eventId: Long): ResponseEntity<Void> {
        AuthUtils.checkIsUser()

        val userId = AuthUtils.extractUserInfo().email

        this.eventService.deleteEventById(userId, eventId);

        return ResponseEntity.noContent().build()
    }
}