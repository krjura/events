package org.krjura.projects.events.events

data class ReminderCreated(val userId: String, val eventId: Long, val reminderId: Long)