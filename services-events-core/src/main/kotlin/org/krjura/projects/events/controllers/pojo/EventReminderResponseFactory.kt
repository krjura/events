package org.krjura.projects.events.controllers.pojo

import org.krjura.projects.events.model.Reminder
import java.time.format.DateTimeFormatter

object EventReminderResponseFactory {

    fun of(reminder: Reminder): EventReminderResponse {
        return EventReminderResponse(
                id = reminder.id!!,
                userId = reminder.userId,
                eventId = reminder.eventId,
                eventTime = reminder.eventTime.format(DateTimeFormatter.ISO_TIME),
                occursBefore = reminder.occursBefore
        )
    }
}