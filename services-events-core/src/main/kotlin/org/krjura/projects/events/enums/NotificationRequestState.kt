package org.krjura.projects.events.enums

enum class NotificationRequestState {
    OPEN,
    ASSIGNED,
    PROCESSED
}