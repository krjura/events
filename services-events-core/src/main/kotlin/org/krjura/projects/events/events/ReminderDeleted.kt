package org.krjura.projects.events.events

data class ReminderDeleted(val userId: String, val eventId: Long, val reminderId: Long)