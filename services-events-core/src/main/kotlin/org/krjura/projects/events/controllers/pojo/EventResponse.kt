package org.krjura.projects.events.controllers.pojo

data class EventResponse(
        val id: Long,
        val userId: String,
        val beginAt: String,
        val occursEvery: String,
        val eventTime: String,
        val nextOccurrence: String,
        val personInfo: String,
        val note: String,
        val useReminder: Boolean
)