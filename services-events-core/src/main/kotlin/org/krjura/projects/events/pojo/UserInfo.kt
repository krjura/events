package org.krjura.projects.events.pojo

data class UserInfo (
        val email: String,
        val privileges: List<String>
)