package org.krjura.projects.events

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableConfigurationProperties
@EnableScheduling
open class EventsParentApplication

fun main(args: Array<String>) {
	runApplication<EventsParentApplication>(*args)
}
