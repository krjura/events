package org.krjura.projects.events.pojo

import org.krjura.projects.events.ex.EventProcessingException

object OccursParser {

    fun parse(value: String): Occurs {
        val elements = value.split(" ")

        if(elements.isEmpty() || elements.size != 2) {
            throw EventProcessingException("invalid occurs string got $value")
        }

        return Occurs(elements[0].toInt(), OccursTimeUnit.valueOf(elements[1]))
    }
}