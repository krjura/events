package org.krjura.projects.events.configprops

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "krjura.events")
open class ApplicationProps(
    var monitoring: MonitoringProps = MonitoringProps(),
    var scheduling: SchedulingProps = SchedulingProps()
)