package org.krjura.projects.events.model

import org.krjura.projects.events.enums.NotificationRequestState
import org.krjura.projects.events.enums.NotificationType
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDateTime

@Table("notification_request")
data class NotificationRequest(
        @Id val id: Long?,
        @Column("reminder_ref") val reminderId: Long,
        @Column("state") val state: NotificationRequestState,
        @Column("type") val type: NotificationType,
        @Column("attempts") val attempts: Int,
        @Column("execute_on") val executeOn: LocalDateTime)