package org.krjura.projects.events.enums

enum class Privileges {

    ROLE_MONITORING,
    ROLE_USER
}