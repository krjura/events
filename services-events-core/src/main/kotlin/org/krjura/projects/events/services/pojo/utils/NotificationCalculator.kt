package org.krjura.projects.events.services.pojo.utils

import org.krjura.projects.events.pojo.OccursParser
import org.krjura.projects.events.pojo.OccursTimeUnit
import java.time.LocalDateTime

object NotificationCalculator {

    fun calculate(eventTime: LocalDateTime, occursBefore: String): LocalDateTime {
        val occurs = OccursParser.parse(occursBefore)

        return when(occurs.type) {
            OccursTimeUnit.YEAR -> eventTime.minusYears(occurs.duration.toLong())
            OccursTimeUnit.MONTH -> eventTime.minusMonths(occurs.duration.toLong())
            OccursTimeUnit.WEEK -> eventTime.minusWeeks(occurs.duration.toLong())
            OccursTimeUnit.DAY -> eventTime.minusDays(occurs.duration.toLong())
        }
    }
}