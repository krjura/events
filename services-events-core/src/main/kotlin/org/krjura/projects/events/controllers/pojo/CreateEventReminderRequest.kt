package org.krjura.projects.events.controllers.pojo

import org.krjura.projects.events.validations.OccursConstraint
import org.krjura.projects.events.validations.TimeConstraint

data class CreateEventReminderRequest (
        @TimeConstraint val eventTime: String,
        @OccursConstraint val occursBefore: String
)