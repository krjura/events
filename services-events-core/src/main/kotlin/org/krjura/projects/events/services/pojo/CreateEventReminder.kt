package org.krjura.projects.events.services.pojo

import org.krjura.projects.events.pojo.Occurs
import java.time.LocalTime

data class CreateEventReminder (
        val userId: String,
        val eventId: Long,
        val eventTime: LocalTime,
        val occursBefore: Occurs
)