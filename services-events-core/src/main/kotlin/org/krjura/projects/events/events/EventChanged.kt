package org.krjura.projects.events.events

data class EventChanged(val userId: String, val eventId: Long)