package org.krjura.projects.events.controllers.pojo

import org.krjura.projects.events.validations.DateConstraint
import org.krjura.projects.events.validations.OccursConstraint
import org.krjura.projects.events.validations.TimeConstraint
import org.krjura.projects.events.validations.VarcharConstraint

data class CreateEventRequest(
        @DateConstraint val beginAt: String,
        @OccursConstraint val occursEvery: String,
        @TimeConstraint val eventTime: String,
        @VarcharConstraint val personInfo: String,
        @VarcharConstraint val note: String,
        val useReminder: Boolean
)