package org.krjura.projects.events.controllers.graphql

import graphql.execution.DataFetcherResult
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.krjura.projects.events.controllers.pojo.EventReminderResponse
import org.krjura.projects.events.controllers.pojo.EventReminderResponseFactory
import org.krjura.projects.events.controllers.pojo.EventResponse
import org.krjura.projects.events.controllers.pojo.EventResponseFactory
import org.krjura.projects.events.ex.GraphQlException
import org.krjura.projects.events.services.EventService
import org.krjura.projects.events.controllers.graphql.context.EventListingContext
import org.krjura.projects.events.utils.AuthUtils
import org.springframework.data.domain.PageRequest
import org.springframework.security.authentication.InsufficientAuthenticationException
import java.util.Optional
import java.util.stream.Collectors

class GraphQlDataFetchers(private val eventService: EventService) {

    fun eventsByUserFetcher(): DataFetcher<DataFetcherResult<List<EventResponse>>> {
        return DataFetcher { environment -> getAllEventsByUserId(environment) }
    }

    fun eventById(): DataFetcher<DataFetcherResult<Optional<EventResponse>>> {
        return DataFetcher { environment -> getEventById(environment) }
    }

    fun remindersByEventId(): DataFetcher<DataFetcherResult<List<EventReminderResponse>>> {
        return DataFetcher { environment -> getRemindersByEventId(environment) }
    }

    private fun getEventById(environment: DataFetchingEnvironment): DataFetcherResult<Optional<EventResponse>> {
        checkIsUserForGraphQl()

        val userId = AuthUtils.extractUserInfo().email
        val id: Int = environment.arguments["id"] as Int

        val event = eventService
                .findEventById(userId, id.toLong())
                .map { EventResponseFactory.of(it) }

        return DataFetcherResult
                .newResult<Optional<EventResponse>>()
                .data(event)
                .localContext(EventListingContext(loaded = false))
                .build()
    }

    private fun getAllEventsByUserId(environment: DataFetchingEnvironment): DataFetcherResult<List<EventResponse>> {
        checkIsUserForGraphQl()

        val userId = AuthUtils.extractUserInfo().email

        val page: Int = environment.arguments.getOrDefault("page", 0) as Int
        val size: Int = environment.arguments.getOrDefault("size", 50) as Int
        val searchText = environment.arguments.getOrDefault("searchText", "") as String

        val events = findAllEvents(userId, searchText, page, size)

        return DataFetcherResult
                .newResult<List<EventResponse>>()
                .data(events)
                .localContext(determineLocalContextForEventListing(environment, events, userId))
                .build()
    }

    private fun findAllEvents(userId: String, searchText: String, page: Int, size: Int): List<EventResponse> {
        return if (searchText.isEmpty()) {
            eventService
                    .findAllEventsByUserId(userId, PageRequest.of(page, size))
                    .map { EventResponseFactory.of(it) }
        } else {
            eventService
                    .findAllEventsByUserIdAndSearchText(userId, searchText, PageRequest.of(page, size))
                    .map { EventResponseFactory.of(it) }
        }
    }

    private fun determineLocalContextForEventListing(
            environment: DataFetchingEnvironment, events: List<EventResponse>, userId: String): EventListingContext {

        // if reminders are required load them now
        return if (environment.selectionSet.contains("reminders")) {
            val ids = events
                    .stream()
                    .map { it.id }
                    .collect(Collectors.toList())

            val reminders = this
                    .eventService
                    .findRemindersByEventIdsAndUserId(userId, ids)

            EventListingContext(loaded = true, reminders = reminders);
        } else {
            EventListingContext(loaded = false)
        }
    }

    private fun getRemindersByEventId(env: DataFetchingEnvironment): DataFetcherResult<List<EventReminderResponse>> {
        checkIsUserForGraphQl()

        val source = env.getSource<EventResponse>()
        val context = env.getLocalContext() as EventListingContext;

        val reminders: List<EventReminderResponse> = if(context.loaded) {
            context
                    .reminders
                    .filter { it.userId == source.userId && it.eventId == source.id }
                    .map { EventReminderResponseFactory.of(it) }
        } else {
            this
                    .eventService.findRemindersByEventIdAndUserId(source.userId, source.id)
                    .map { EventReminderResponseFactory.of(it) }
        }

        return DataFetcherResult.newResult<List<EventReminderResponse>>().data(reminders).build()
    }

    private fun checkIsUserForGraphQl() {
        try {
            AuthUtils.checkIsUser()
        } catch (e: InsufficientAuthenticationException) {
            throw GraphQlException(e.message, e)
        }
    }


}