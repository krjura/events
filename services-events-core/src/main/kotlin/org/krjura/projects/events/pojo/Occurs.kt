package org.krjura.projects.events.pojo

data class Occurs(
        val duration: Int,
        val type: OccursTimeUnit) {

    override fun toString(): String {
        return duration.toString() + " " + type.name
    }

    fun isValid(): Boolean {
        return duration >= 0;
    }
}