package org.krjura.projects.events.pojo

enum class OccursTimeUnit {
    DAY,
    WEEK,
    MONTH,
    YEAR
}