package org.krjura.projects.events.ex.response

data class ErrorResponse(
        val statusDescription: String,
        val data: List<ErrorDetails>
)