package org.krjura.projects.events.repository

import org.krjura.projects.events.model.NotificationRequest
import org.springframework.data.jdbc.repository.query.Modifying
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import java.util.Optional

interface NotificationRequestRepository : CrudRepository<NotificationRequest, Long> {

    @Modifying
    @Query("DELETE FROM notification_request e WHERE e.reminder_ref = :reminderId")
    fun deleteByReminderId(@Param("reminderId") reminderId: Long)

    @Query("SELECT e.* FROM notification_request e WHERE e.reminder_ref = :reminderId")
    fun findByReminderId(@Param("reminderId") reminderId: Long): Optional<NotificationRequest>

    @Query("SELECT e.* FROM notification_request e")
    override fun findAll(): List<NotificationRequest>
}