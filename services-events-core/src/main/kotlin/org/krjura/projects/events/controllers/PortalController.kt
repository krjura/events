package org.krjura.projects.events.controllers

import org.springframework.core.io.FileSystemResource
import org.springframework.core.io.Resource
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import java.nio.file.Paths

@Controller
class PortalController {

    @GetMapping(value = ["/", "/portal", "/portal/events/**"])
    fun rootRedirect(): ResponseEntity<Resource> {
        return ResponseEntity.ok(FileSystemResource(Paths.get("web-resources/index.html")))
    }

    @GetMapping(value = ["/favicon.ico"])
    fun favicon(): ResponseEntity<Resource> {
        return ResponseEntity.ok(FileSystemResource(Paths.get("web-resources/favicon.ico")))
    }
}