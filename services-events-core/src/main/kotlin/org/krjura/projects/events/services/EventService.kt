package org.krjura.projects.events.services

import org.krjura.projects.events.eventhub.EventBus
import org.krjura.projects.events.events.EventChanged
import org.krjura.projects.events.events.ReminderDeleted
import org.krjura.projects.events.events.ReminderEventFactory
import org.krjura.projects.events.ex.EventProcessingException
import org.krjura.projects.events.model.Event
import org.krjura.projects.events.model.Reminder
import org.krjura.projects.events.pojo.UserInfo
import org.krjura.projects.events.repository.ReminderRepository
import org.krjura.projects.events.repository.EventRepository
import org.krjura.projects.events.services.pojo.CreateEvent
import org.krjura.projects.events.services.pojo.CreateEventReminder
import org.krjura.projects.events.services.pojo.utils.OccurrenceCalculator
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.util.Optional

@Service
class EventService(
        private val eventRepository: EventRepository,
        private val reminderRepository: ReminderRepository,
        private val eventBus: EventBus) {

    fun createEvent(userInfo: UserInfo, createEvent: CreateEvent) {

        val event = Event(
                null,
                userInfo.email,
                createEvent.beginAt,
                createEvent.occursEvery.toString(),
                createEvent.eventTime,
                OccurrenceCalculator.default.calculate(createEvent.beginAt, createEvent.eventTime, createEvent.occursEvery),
                createEvent.personInfo,
                createEvent.note,
                createEvent.useReminder
        )

        val saved = this.eventRepository.save(event)
        this.eventBus.send(EventChanged(userInfo.email, saved.id!!))
    }

    fun findEventById(userId: String, id: Long): Optional<Event> {
        return this.eventRepository.findByUserIdAndId(userId, id)
    }

    fun findAllEventsByUserId(userId: String, pageable: Pageable): List<Event> {
        return this.eventRepository.findAllByUserId(userId, pageable.pageSize, pageable.offset)
    }

    fun findAllEventsByUserIdAndSearchText(
            userId: String, searchText: String, pageable: Pageable): List<Event> {

        return this
                .eventRepository
                .findAllEventsByUserIdAndSearchText(userId, searchText, pageable.pageSize, pageable.offset)
    }

    fun deleteEventById(userId: String, eventId: Long) {
        this.reminderRepository.deleteRemindersByUserIdAndEventId(userId, eventId)
        this.eventRepository.deleteEventById(userId, eventId)
    }

    fun createEventReminder(request: CreateEventReminder) {
        val eventListingOptional = findEventById(request.userId, request.eventId)

        if(eventListingOptional.isEmpty) {
            throw EventProcessingException("event with id of ${request.eventId} not found")
        }

        var model = Reminder(
                id = null,
                userId = request.userId,
                eventId = request.eventId,
                eventTime = request.eventTime,
                occursBefore = request.occursBefore.toString()
        )

        model = this.reminderRepository.save(model)
        this.eventBus.send(ReminderEventFactory.created(model))
    }
    fun updateEvent(userInfo: UserInfo, eventId: Long, createEvent: CreateEvent) {
        this
                .eventRepository
                .findByUserIdAndId(userId = userInfo.email, id = eventId)
                .orElseThrow { EventProcessingException("event with id of $eventId does not exist" ) }

        this
                .eventRepository
                .update(
                        beginAt = createEvent.beginAt,
                        occursEvery = createEvent.occursEvery.toString(),
                        eventTime = createEvent.eventTime,
                        nextOccurrence = OccurrenceCalculator.default.calculate(createEvent.beginAt, createEvent.eventTime, createEvent.occursEvery),
                        personInfo = createEvent.personInfo,
                        note = createEvent.note,
                        userId = userInfo.email,
                        eventId = eventId,
                        useReminder = false
                )

        this.eventBus.send(EventChanged(userInfo.email, eventId))
    }


    fun findRemindersByEventIdAndUserId(userId: String, eventId: Long): List<Reminder> {
        return this.reminderRepository.findReminderByEventIdAndUserId(userId, eventId)
    }

    fun deleteReminderByIdAndUserId(userId: String, eventId: Long, reminderId: Long) {
        this.reminderRepository.deleteReminderByIdAndUserId(userId, reminderId)
        this.eventBus.send(ReminderDeleted(userId, eventId, reminderId))
    }

    fun findRemindersByEventIdsAndUserId(userId: String, eventIds: List<Long>): List<Reminder> {
        return this.reminderRepository.findRemindersByEventIdsAndUserId(userId, eventIds)
    }
}