package org.krjura.projects.events.ex;

import graphql.ErrorClassification;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class GraphQlException extends RuntimeException implements GraphQLError {

    public GraphQlException(String message) {
        super(message);
    }

    public GraphQlException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorClassification getErrorType() {
        return ErrorType.DataFetchingException;
    }

    @Override
    public List<Object> getPath() {
        return null;
    }

    @Override
    public Map<String, Object> toSpecification() {
        return null;
    }

    @Override
    public Map<String, Object> getExtensions() {
        return getCause() == null ? Collections.emptyMap() : Map.of("exception", getCause().getClass().getSimpleName());
    }
}
