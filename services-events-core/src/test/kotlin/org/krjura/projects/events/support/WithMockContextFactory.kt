package org.krjura.projects.events.support

import org.krjura.projects.events.pojo.UserInfo
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.security.oauth2.core.oidc.OidcIdToken
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser
import org.springframework.security.test.context.support.WithSecurityContextFactory
import java.time.Instant
import java.util.UUID
import java.util.stream.Collectors

class WithMockContextFactory: WithSecurityContextFactory<WithMockUser> {

    companion object {
        const val CONST_DEFAULT_CREDENTIALS = "N/A"
    }

    override fun createSecurityContext(annotation: WithMockUser): SecurityContext {

        val privileges = annotation.privileges.map { it.name }
        val userInfo = UserInfo(annotation.userId, privileges);
        val grantedAuthority = asGrantedAuthorities(userInfo)

        val tokenId = UUID.randomUUID().toString();
        val claims = mapOf(
                Pair("email", annotation.userId),
                Pair("given_name", "tester"),
                Pair("family_name", "c"),
                Pair("sub", tokenId)
        )

        val token = OidcIdToken(tokenId, Instant.now(), Instant.now().plusSeconds(3600), claims)
        val oauthUser = DefaultOidcUser(grantedAuthority, token)
        val authentication = OAuth2AuthenticationToken(oauthUser, grantedAuthority, UUID.randomUUID().toString())

        val context = SecurityContextHolder.createEmptyContext()
        context.authentication = authentication

        return context
    }

    private fun asGrantedAuthorities(userInfo: UserInfo): List<GrantedAuthority> {
        return userInfo
                .privileges
                .stream()
                .map { SimpleGrantedAuthority(it) }
                .collect(Collectors.toList())
    }
}