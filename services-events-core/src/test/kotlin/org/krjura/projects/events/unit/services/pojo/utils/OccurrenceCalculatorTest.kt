package org.krjura.projects.events.unit.services.pojo.utils

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.krjura.projects.events.pojo.Occurs
import org.krjura.projects.events.pojo.OccursTimeUnit
import org.krjura.projects.events.services.pojo.utils.OccurrenceCalculator
import org.krjura.projects.events.services.pojo.utils.SpecificTimeProvider
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@DisplayName("verify occurrence calculator")
class OccurrenceCalculatorTest {

    private val calculator: OccurrenceCalculator =
            OccurrenceCalculator(SpecificTimeProvider(LocalDateTime.of(2019, 6, 18, 17, 23, 0)))

    @DisplayName("verify calculator works when using YEAR as time unit")
    @Test
    fun testYear() {
        // simple
        assertThat(calculator
                .calculate(LocalDate.of(2013, 1, 1), LocalTime.of(9, 0, 0), Occurs(1, OccursTimeUnit.YEAR))
        ).isEqualTo(LocalDateTime.of(2020, 1, 1, 9, 0, 0))

        // skip few units
        assertThat(calculator
                .calculate(LocalDate.of(2013, 1, 1), LocalTime.of(9, 0, 0), Occurs(2, OccursTimeUnit.YEAR))
        ).isEqualTo(LocalDateTime.of(2021, 1, 1, 9, 0, 0))

        // large skip
        assertThat(calculator
                .calculate(LocalDate.of(2013, 1, 1), LocalTime.of(9, 0, 0), Occurs(5, OccursTimeUnit.YEAR))
        ).isEqualTo(LocalDateTime.of(2023, 1, 1, 9, 0, 0))

        // in the future
        val now = LocalDate.now().plusDays(1)
        assertThat(calculator
                .calculate(now, LocalTime.of(9, 0, 0), Occurs(5, OccursTimeUnit.YEAR))
        ).isEqualTo(now.atTime(9, 0, 0))
    }

    @DisplayName("verify calculator works when using MONTH as time unit")
    @Test
    fun testMonth() {
        // simple
        assertThat(calculator
                .calculate(LocalDate.of(2013, 1, 1), LocalTime.of(9, 0, 0), Occurs(1, OccursTimeUnit.MONTH))
        ).isEqualTo(LocalDateTime.of(2019, 7, 1, 9, 0, 0))

        // skip few units
        assertThat(calculator
                .calculate(LocalDate.of(2013, 1, 1), LocalTime.of(9, 0, 0), Occurs(2, OccursTimeUnit.MONTH))
        ).isEqualTo(LocalDateTime.of(2019, 7, 1, 9, 0, 0))

        // large skip
        assertThat(calculator
                .calculate(LocalDate.of(2013, 1, 1), LocalTime.of(9, 0, 0), Occurs(6, OccursTimeUnit.MONTH))
        ).isEqualTo(LocalDateTime.of(2019, 7, 1, 9, 0, 0))

        // in the future
        val now = LocalDate.now().plusDays(1)
        assertThat(calculator
                .calculate(now, LocalTime.of(9, 0, 0), Occurs(5, OccursTimeUnit.MONTH))
        ).isEqualTo(now.atTime(9, 0, 0))
    }

    @DisplayName("verify calculator works when using WEEK as time unit")
    @Test
    fun testWeek() {
        // simple
        assertThat(calculator
                .calculate(LocalDate.of(2019, 6, 17), LocalTime.of(9, 0, 0), Occurs(1, OccursTimeUnit.WEEK))
        ).isEqualTo(LocalDateTime.of(2019, 6, 24, 9, 0, 0))

        // skip few units
        assertThat(calculator
                .calculate(LocalDate.of(2019, 6, 17), LocalTime.of(9, 0, 0), Occurs(2, OccursTimeUnit.WEEK))
        ).isEqualTo(LocalDateTime.of(2019, 7, 1, 9, 0, 0))

        // large skip
        assertThat(calculator
                .calculate(LocalDate.of(2019, 6, 17), LocalTime.of(9, 0, 0), Occurs(6, OccursTimeUnit.WEEK))
        ).isEqualTo(LocalDateTime.of(2019, 7, 29, 9, 0, 0))

        // in the future
        val now = LocalDate.now().plusDays(1)
        assertThat(calculator
                .calculate(now, LocalTime.of(9, 0, 0), Occurs(5, OccursTimeUnit.WEEK))
        ).isEqualTo(now.atTime(9, 0, 0))
    }

    @DisplayName("verify calculator works when using DAY as time unit")
    @Test
    fun testDay() {
        // simple
        assertThat(calculator
                .calculate(LocalDate.of(2019, 6, 18), LocalTime.of(9, 0, 0), Occurs(1, OccursTimeUnit.DAY))
        ).isEqualTo(LocalDateTime.of(2019, 6, 19, 9, 0, 0))

        // skip few units
        assertThat(calculator
                .calculate(LocalDate.of(2019, 6, 18), LocalTime.of(9, 0, 0), Occurs(2, OccursTimeUnit.DAY))
        ).isEqualTo(LocalDateTime.of(2019, 6, 20, 9, 0, 0))

        // large skip
        assertThat(calculator
                .calculate(LocalDate.of(2019, 6, 18), LocalTime.of(9, 0, 0), Occurs(6, OccursTimeUnit.DAY))
        ).isEqualTo(LocalDateTime.of(2019, 6, 24, 9, 0, 0))

        // in the future
        val now = LocalDate.now().plusDays(1)
        assertThat(calculator
                .calculate(now, LocalTime.of(9, 0, 0), Occurs(5, OccursTimeUnit.DAY))
        ).isEqualTo(now.atTime(9, 0, 0))
    }
}