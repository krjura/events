package org.krjura.projects.events.support

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MvcResult
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths

@ExtendWith(SpringExtension::class)
@ProjectTest
abstract class TestBase {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var databaseSupportService: DatabaseSupportService

    @BeforeEach
    fun beforeTestBase() {
        this.databaseSupportService.deleteAll()
    }

    @Throws(IOException::class)
    protected fun <T> fromJson(result: MvcResult, clazz: Class<T>): T {
        return fromJson(result.response.contentAsString, clazz)
    }

    @Throws(IOException::class)
    protected fun <T> fromJson(content: String, clazz: Class<T>): T {
        return this.objectMapper.readValue(content, clazz)
    }

    protected fun toJson(data: Any): String {
        return this.objectMapper.writeValueAsString(data)
    }

    fun contentOf(fileName: String): String {
        return String(Files.readAllBytes(Paths.get(javaClass.classLoader.getResource(fileName)!!.toURI())))
    }
}