package org.krjura.projects.events.support

import org.junit.jupiter.api.BeforeEach
import org.springframework.web.context.WebApplicationContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder

@ProjectTest
abstract class WebTestBase: TestBase() {

    @Autowired
    lateinit var context: WebApplicationContext

    lateinit var mockMvc:MockMvc

    @BeforeEach
    fun beforeWebTestBase() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.context)
                .apply<DefaultMockMvcBuilder>(SecurityMockMvcConfigurers.springSecurity())
                .build()
    }
}