package org.krjura.projects.events.integration.services

import org.junit.jupiter.api.Test
import org.krjura.projects.events.events.ReminderCreated
import org.krjura.projects.events.model.ModelTestUtils
import org.krjura.projects.events.repository.ReminderRepository
import org.krjura.projects.events.repository.EventRepository
import org.krjura.projects.events.repository.NotificationRequestRepository
import org.krjura.projects.events.services.NotificationService
import org.krjura.projects.events.support.TestBase
import org.springframework.beans.factory.annotation.Autowired
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.krjura.projects.events.enums.NotificationRequestState
import org.krjura.projects.events.enums.NotificationType
import org.krjura.projects.events.events.ReminderDeleted
import org.krjura.projects.events.events.ReminderEventFactory
import java.time.LocalDateTime

@DisplayName("verify handling of events by NotificationService")
class NotificationServiceTest : TestBase() {

    companion object {
        const val defaultUserId = "tester@example.com";
    }

    @Autowired
    lateinit var eventRepository: EventRepository

    @Autowired
    lateinit var reminderRepository: ReminderRepository

    @Autowired
    lateinit var notificationRepository: NotificationRequestRepository

    @Autowired
    lateinit var notificationService: NotificationService

    @DisplayName("verify processing of reminder created event")
    @Test
    fun testProcessingOfReminderCreated() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)
        val reminder = ModelTestUtils.defaultEventReminder(reminderRepository, defaultUserId, eventListing)

        this.notificationService.onEvent(ReminderEventFactory.created(reminder))

        val notifications = notificationRepository.findAll()

        assertThat(notifications).hasSize(1)
        assertThat(notifications[0].reminderId).isEqualTo(reminder.id!!)
        assertThat(notifications[0].type).isEqualTo(NotificationType.REMINDER)
        assertThat(notifications[0].state).isEqualTo(NotificationRequestState.OPEN)
        assertThat(notifications[0].attempts).isEqualTo(0)
        assertThat(notifications[0].executeOn).isEqualTo(LocalDateTime.of(2019, 12, 31, 9, 0, 0, 0))
    }

    @DisplayName("verify processing of reminder created event when reminder does not exists")
    @Test
    fun testProcessingOfReminderCreatedWhenReminderDoesNotExists() {
        this.notificationService.onEvent(ReminderCreated(defaultUserId, 1, 1))

        val notifications = notificationRepository.findAll()
        assertThat(notifications).hasSize(0)
    }

    @DisplayName("verify processing of reminder created event when notification already exists")
    @Test
    fun testProcessingOfReminderCreatedWhenNotificationAlreadyExists() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)
        val reminder = ModelTestUtils.defaultEventReminder(reminderRepository, defaultUserId, eventListing)
        ModelTestUtils.defaultNotification(reminder, this.notificationRepository)

        this.notificationService.onEvent(ReminderEventFactory.created(reminder))

        val notifications = notificationRepository.findAll()
        assertThat(notifications).hasSize(1)
        assertThat(notifications[0].reminderId).isEqualTo(reminder.id!!)
        assertThat(notifications[0].type).isEqualTo(NotificationType.REMINDER)
        assertThat(notifications[0].state).isEqualTo(NotificationRequestState.OPEN)
        assertThat(notifications[0].attempts).isEqualTo(0)
        assertThat(notifications[0].executeOn).isEqualTo(LocalDateTime.of(2019, 12, 31, 9, 0, 0, 0))
    }

    @Test
    @DisplayName("verify processing of reminder deleted event")
    fun testProcessingOfReminderDeleted() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)
        val reminder = ModelTestUtils.defaultEventReminder(reminderRepository, defaultUserId, eventListing)
        ModelTestUtils.defaultNotification(reminder, this.notificationRepository)

        this.notificationService.onEvent(ReminderEventFactory.deleted(reminder))

        val notifications = notificationRepository.findAll()
        assertThat(notifications).hasSize(0)
    }

    @Test
    @DisplayName("verify processing of reminder deleted event when there is no notification")
    fun testProcessingOfReminderDeletedWhenThereIsNoNotification() {
        this.notificationService.onEvent(ReminderDeleted(defaultUserId, 1, 1))

        val notifications = notificationRepository.findAll()
        assertThat(notifications).hasSize(0)
    }
}