package org.krjura.projects.events.integration.controllers

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.krjura.projects.events.enums.Privileges
import org.krjura.projects.events.model.ModelTestUtils
import org.krjura.projects.events.repository.ReminderRepository
import org.krjura.projects.events.repository.EventRepository
import org.krjura.projects.events.support.WebTestBase
import org.krjura.projects.events.support.WithMockUser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.request
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.format.DateTimeFormatter
import org.hamcrest.CoreMatchers.`is` as matchIs

@DisplayName("verify graph QL endpoint")
class GraphQlControllerTest : WebTestBase() {

    companion object {
        const val defaultUserId = "tester@example.com"
    }

    @Autowired
    lateinit var eventRepository: EventRepository

    @Autowired
    lateinit var reminderRepository: ReminderRepository

    @DisplayName("verify graphQL endpoint cannot fetch events by id when not authorized")
    @Test
    @WithMockUser(privileges = [Privileges.ROLE_MONITORING])
    fun testGetEventsByIdWhenNotAuthorized() {
        // when
        val request = contentOf("graph-ql/get-event-by-id.query").replace("\$id", "1")

        val result = mockMvc.perform(
                post("/graphql").content(request).contentType("application/graphql"))
                .andExpect(status().isOk)
                .andReturn()

        // then
        this.mockMvc.perform(asyncDispatch(result))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("errors.length()", matchIs(equalTo(1))))
                .andExpect(jsonPath("errors[0].extensions.exception", matchIs(equalTo("InsufficientAuthenticationException"))))
                .andExpect(jsonPath("errors[0].extensions.classification", matchIs(equalTo("DataFetchingException"))))
                .andReturn()
    }

    @DisplayName("verify graphQL endpoint cannot fetch events by id when not logged in")
    @Test
    fun testGetEventsByIdWhenNotLoggedIn() {
        // when and then
        val request = contentOf("graph-ql/get-event-by-id.query").replace("\$id", "1")

        mockMvc.perform(
                post("/graphql").content(request).contentType("application/graphql"))
                .andExpect(status().isForbidden)
    }

    @DisplayName("verify graphQL endpoint can fetch events by id with reminders")
    @Test
    @WithMockUser
    fun testGetEventsByIdWithReminders() {
        // given
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)
        val firstReminder = ModelTestUtils.defaultEventReminder(reminderRepository, defaultUserId, eventListing)
        val secondReminder = ModelTestUtils.defaultEventReminder(reminderRepository, defaultUserId, eventListing)

        // when
        val request = contentOf("graph-ql/get-event-by-id-with-reminders.query").replace("\$id", eventListing.id.toString())

        val result = mockMvc.perform(
                post("/graphql").content(request).contentType("application/graphql"))
                .andExpect(status().isOk)
                .andExpect(request().asyncStarted())
                .andReturn()

        // then
        this.mockMvc.perform(asyncDispatch(result))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("data.eventsById", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsById.id", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsById.beginAt", matchIs(equalTo(eventListing.beginAt.format(DateTimeFormatter.ISO_DATE)))))
                .andExpect(jsonPath("data.eventsById.occursEvery", matchIs(equalTo(eventListing.occursEvery))))
                .andExpect(jsonPath("data.eventsById.eventTime", matchIs(equalTo(eventListing.eventTime.format(DateTimeFormatter.ISO_TIME)))))
                .andExpect(jsonPath("data.eventsById.personInfo", matchIs(equalTo(eventListing.personInfo))))
                .andExpect(jsonPath("data.eventsById.note", matchIs(equalTo(eventListing.note))))
                .andExpect(jsonPath("data.eventsById.useReminder", matchIs(equalTo(eventListing.useReminder))))
                .andExpect(jsonPath("data.eventsById.reminders.length()", matchIs(equalTo(2))))
                .andExpect(jsonPath("data.eventsById.reminders[0].eventTime", matchIs(equalTo(firstReminder.eventTime.format(DateTimeFormatter.ISO_TIME)))))
                .andExpect(jsonPath("data.eventsById.reminders[0].occursBefore", matchIs(equalTo(firstReminder.occursBefore))))
                .andExpect(jsonPath("data.eventsById.reminders[1].eventTime", matchIs(equalTo(secondReminder.eventTime.format(DateTimeFormatter.ISO_TIME)))))
                .andExpect(jsonPath("data.eventsById.reminders[1].occursBefore", matchIs(equalTo(secondReminder.occursBefore))))
    }

    @DisplayName("verify graphQL endpoint can fetch events by id")
    @Test
    @WithMockUser
    fun testGetEventsById() {
        // given
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        // when
        val request = contentOf("graph-ql/get-event-by-id.query").replace("\$id", eventListing.id.toString())

        val result = mockMvc.perform(
                post("/graphql").content(request).contentType("application/graphql"))
                .andExpect(status().isOk)
                .andExpect(request().asyncStarted())
                .andReturn()

        // then
        this.mockMvc.perform(asyncDispatch(result))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("data.eventsById", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsById.id", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsById.beginAt", matchIs(equalTo(eventListing.beginAt.format(DateTimeFormatter.ISO_DATE)))))
                .andExpect(jsonPath("data.eventsById.occursEvery", matchIs(equalTo(eventListing.occursEvery))))
                .andExpect(jsonPath("data.eventsById.eventTime", matchIs(equalTo(eventListing.eventTime.format(DateTimeFormatter.ISO_TIME)))))
                .andExpect(jsonPath("data.eventsById.personInfo", matchIs(equalTo(eventListing.personInfo))))
                .andExpect(jsonPath("data.eventsById.note", matchIs(equalTo(eventListing.note))))
                .andExpect(jsonPath("data.eventsById.useReminder", matchIs(equalTo(eventListing.useReminder))))
    }

    @DisplayName("verify graphQL endpoint cannot fetch events by id belonging to some other user")
    @Test
    @WithMockUser(userId = "tester2@example.com")
    fun testGetEventsWhenOtherUser() {
        // given
        ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        // when
        val request = contentOf("graph-ql/get-all-events-by-user.query")

        val result = mockMvc.perform(
                post("/graphql").content(request).contentType("application/graphql"))
                .andExpect(status().isOk)
                .andExpect(request().asyncStarted())
                .andReturn()

        // then
        this.mockMvc.perform(asyncDispatch(result))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("data", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsByUser.length()", matchIs(equalTo(0))))
                .andReturn()
    }

    @DisplayName("verify graphQL endpoint cannot fetch all events when not authorized")
    @Test
    @WithMockUser(privileges = [Privileges.ROLE_MONITORING])
    fun testGetEventsWhenNotAuthorized() {
        // when
        val request = contentOf("graph-ql/get-all-events-by-user.query")

        val result = mockMvc.perform(
                post("/graphql").content(request).contentType("application/graphql"))
                .andExpect(status().isOk)
                .andReturn()

        // then
        this.mockMvc.perform(asyncDispatch(result))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("errors.length()", matchIs(equalTo(1))))
                .andExpect(jsonPath("errors[0].extensions.exception", matchIs(equalTo("InsufficientAuthenticationException"))))
                .andExpect(jsonPath("errors[0].extensions.classification", matchIs(equalTo("DataFetchingException"))))
                .andReturn()
    }

    @DisplayName("verify graphQL endpoint cannot fetch all events when not logged in")
    @Test
    fun testGetEventsWhenNotLoggedIn() {
        // when and then
        val request = contentOf("graph-ql/get-all-events-by-user.query")

        mockMvc.perform(
                post("/graphql").content(request).contentType("application/graphql"))
                .andExpect(status().isForbidden)
    }

    @DisplayName("verify graphQL endpoint can fetch all events with reminders")
    @Test
    @WithMockUser
    fun testGetEventsWithReminders() {
        // given
        val firstEvent = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)
        val firstReminder = ModelTestUtils.defaultEventReminder(reminderRepository, defaultUserId, firstEvent)

        val secondEvent = ModelTestUtils.defaultSecondEvent(eventRepository, defaultUserId)
        val secondReminder = ModelTestUtils.defaultEventReminder(reminderRepository, defaultUserId, secondEvent)

        // when
        val request = contentOf("graph-ql/get-all-events-by-user-with-reminders.query")

        val result = mockMvc.perform(
                post("/graphql").content(request).contentType("application/graphql"))
                .andExpect(status().isOk)
                .andExpect(request().asyncStarted())
                .andReturn()

        // then
        this.mockMvc.perform(asyncDispatch(result))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("data", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsByUser", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsByUser[0].id", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsByUser[0].beginAt", matchIs(equalTo(secondEvent.beginAt.format(DateTimeFormatter.ISO_DATE)))))
                .andExpect(jsonPath("data.eventsByUser[0].occursEvery", matchIs(equalTo(secondEvent.occursEvery))))
                .andExpect(jsonPath("data.eventsByUser[0].eventTime", matchIs(equalTo(secondEvent.eventTime.format(DateTimeFormatter.ISO_TIME)))))
                .andExpect(jsonPath("data.eventsByUser[0].personInfo", matchIs(equalTo(secondEvent.personInfo))))
                .andExpect(jsonPath("data.eventsByUser[0].note", matchIs(equalTo(secondEvent.note))))
                .andExpect(jsonPath("data.eventsByUser[0].useReminder", matchIs(equalTo(secondEvent.useReminder))))
                .andExpect(jsonPath("data.eventsByUser[0].reminders.length()", matchIs(equalTo(1))))
                .andExpect(jsonPath("data.eventsByUser[0].reminders.[0].eventTime", matchIs(secondEvent.eventTime.format(DateTimeFormatter.ISO_TIME))))
                .andExpect(jsonPath("data.eventsByUser[0].reminders.[0].occursBefore", matchIs(secondReminder.occursBefore)))

                .andExpect(jsonPath("data.eventsByUser[1].id", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsByUser[1].beginAt", matchIs(equalTo(firstEvent.beginAt.format(DateTimeFormatter.ISO_DATE)))))
                .andExpect(jsonPath("data.eventsByUser[1].occursEvery", matchIs(equalTo(firstEvent.occursEvery))))
                .andExpect(jsonPath("data.eventsByUser[1].eventTime", matchIs(equalTo(firstEvent.eventTime.format(DateTimeFormatter.ISO_TIME)))))
                .andExpect(jsonPath("data.eventsByUser[1].personInfo", matchIs(equalTo(firstEvent.personInfo))))
                .andExpect(jsonPath("data.eventsByUser[1].note", matchIs(equalTo(firstEvent.note))))
                .andExpect(jsonPath("data.eventsByUser[1].useReminder", matchIs(equalTo(firstEvent.useReminder))))
                .andExpect(jsonPath("data.eventsByUser[1].reminders.length()", matchIs(equalTo(1))))
                .andExpect(jsonPath("data.eventsByUser[1].reminders.[0].eventTime", matchIs(firstReminder.eventTime.format(DateTimeFormatter.ISO_TIME))))
                .andExpect(jsonPath("data.eventsByUser[1].reminders.[0].occursBefore", matchIs(firstReminder.occursBefore)))
    }

    @DisplayName("verify graphQL endpoint can fetch all events when search text is defined")
    @Test
    @WithMockUser
    fun testGetEventsWithSearchText() {
        // given
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        // when
        val request = contentOf("graph-ql/get-all-events-by-user-with-search.query")

        val result = mockMvc.perform(
                post("/graphql").content(request).contentType("application/graphql"))
                .andExpect(status().isOk)
                .andExpect(request().asyncStarted())
                .andReturn()

        println("resp" + result.response.contentAsString)

        // then
        this.mockMvc.perform(asyncDispatch(result))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("data", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsByUser", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsByUser[0].id", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsByUser[0].beginAt", matchIs(equalTo(eventListing.beginAt.format(DateTimeFormatter.ISO_DATE)))))
                .andExpect(jsonPath("data.eventsByUser[0].occursEvery", matchIs(equalTo(eventListing.occursEvery))))
                .andExpect(jsonPath("data.eventsByUser[0].eventTime", matchIs(equalTo(eventListing.eventTime.format(DateTimeFormatter.ISO_TIME)))))
                .andExpect(jsonPath("data.eventsByUser[0].personInfo", matchIs(equalTo(eventListing.personInfo))))
                .andExpect(jsonPath("data.eventsByUser[0].note", matchIs(equalTo(eventListing.note))))
                .andExpect(jsonPath("data.eventsByUser[0].useReminder", matchIs(equalTo(eventListing.useReminder))))
                .andReturn()
    }

    @DisplayName("verify graphQL endpoint can fetch all events")
    @Test
    @WithMockUser
    fun testGetEvents() {
        // given
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        // when
        val request = contentOf("graph-ql/get-all-events-by-user.query")

        val result = mockMvc.perform(
                post("/graphql").content(request).contentType("application/graphql"))
                .andExpect(status().isOk)
                .andExpect(request().asyncStarted())
                .andReturn()

        // then
        this.mockMvc.perform(asyncDispatch(result))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("data", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsByUser", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsByUser[0].id", matchIs(notNullValue())))
                .andExpect(jsonPath("data.eventsByUser[0].beginAt", matchIs(equalTo(eventListing.beginAt.format(DateTimeFormatter.ISO_DATE)))))
                .andExpect(jsonPath("data.eventsByUser[0].occursEvery", matchIs(equalTo(eventListing.occursEvery))))
                .andExpect(jsonPath("data.eventsByUser[0].eventTime", matchIs(equalTo(eventListing.eventTime.format(DateTimeFormatter.ISO_TIME)))))
                .andExpect(jsonPath("data.eventsByUser[0].personInfo", matchIs(equalTo(eventListing.personInfo))))
                .andExpect(jsonPath("data.eventsByUser[0].note", matchIs(equalTo(eventListing.note))))
                .andExpect(jsonPath("data.eventsByUser[0].useReminder", matchIs(equalTo(eventListing.useReminder))))
                .andReturn()
    }
}