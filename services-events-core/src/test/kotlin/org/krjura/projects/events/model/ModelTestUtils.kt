package org.krjura.projects.events.model

import org.krjura.projects.events.enums.NotificationRequestState
import org.krjura.projects.events.enums.NotificationType
import org.krjura.projects.events.repository.ReminderRepository
import org.krjura.projects.events.repository.EventRepository
import org.krjura.projects.events.repository.NotificationRequestRepository
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

object ModelTestUtils {

    fun defaultEvent(repository: EventRepository, userId: String): Event {
        val event = Event(
                null,
                userId,
                LocalDate.of(2019, 1, 1),
                "1 YEAR",
                LocalTime.of(9, 0, 0),
                LocalDateTime.of(2020, 1, 1, 9, 0, 0),
                "tester",
                "dummy event",
                true
        )

        return repository.save(event)
    }

    fun defaultSecondEvent(repository: EventRepository, userId: String): Event {
        val event = Event(
                null,
                userId,
                LocalDate.of(2019, 2, 2),
                "2 YEAR",
                LocalTime.of(10, 0, 0),
                LocalDateTime.of(2019, 1, 1, 9, 0, 0),
                "tester 2",
                "dummy event 2",
                true
        )

        return repository.save(event)
    }

    fun defaultEventReminder(
            repository: ReminderRepository,
            userId: String,
            event: Event): Reminder {

        val reminder = Reminder(
                id = null,
                eventId = event.id!!,
                userId = userId,
                eventTime = LocalTime.of(10, 0, 0),
                occursBefore = "1 DAY"
        )

        return repository.save(reminder)
    }

    fun defaultSecondEventReminder(
            repository: ReminderRepository,
            userId: String,
            event: Event): Reminder {

        val reminder = Reminder(
                id = null,
                eventId = event.id!!,
                userId = userId,
                eventTime = LocalTime.of(11, 0, 0),
                occursBefore = "2 DAY"
        )

        return repository.save(reminder)
    }

    fun defaultNotification(reminder: Reminder, repository: NotificationRequestRepository): NotificationRequest {
        val request = NotificationRequest(
                id = null,
                reminderId = reminder.id!!,
                state = NotificationRequestState.OPEN,
                type = NotificationType.REMINDER,
                attempts = 0,
                executeOn = LocalDateTime.of(2019, 1, 1, 9, 0, 0, 0)
        )

        return repository.save(request)
    }
}