package org.krjura.projects.events.integration.controllers

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.krjura.projects.events.controllers.pojo.CreateEventRequest
import org.krjura.projects.events.enums.Privileges
import org.krjura.projects.events.ex.response.ErrorResponse
import org.krjura.projects.events.model.ModelTestUtils
import org.krjura.projects.events.repository.EventRepository
import org.krjura.projects.events.support.WebTestBase
import org.krjura.projects.events.support.WithMockUser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@DisplayName("verify provisioning of events")
class EventControllerTest : WebTestBase() {

    companion object {
        const val defaultUserId = "tester@example.com"
    }

    @Autowired
    lateinit var eventRepository: EventRepository

    @DisplayName("verify event cannot be updated when not authorized")
    @Test
    @WithMockUser(privileges = [Privileges.ROLE_MONITORING])
    fun testUpdateEventWhenNotAuthorized() {
        val request = CreateEventRequest(
                beginAt = "2019-02-02",
                occursEvery = "2 YEAR",
                eventTime = "10:00:00",
                personInfo = "tester2",
                note = "Testing2 note",
                useReminder = true
        )

        mockMvc.perform(
                put("/api/v1/events/{eventId}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(status().isForbidden)
    }

    @DisplayName("verify event cannot be updated when not logged in")
    @Test
    fun testUpdateEventWhenNotLoggedIn() {
        val request = CreateEventRequest(
                beginAt = "2019-02-02",
                occursEvery = "2 YEAR",
                eventTime = "10:00:00",
                personInfo = "tester2",
                note = "Testing2 note",
                useReminder = true
        )

        mockMvc.perform(
                put("/api/v1/events/{eventId}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(status().isForbidden)
    }

    @DisplayName("verify event cannot be updated when logged in as some other user")
    @Test
    @WithMockUser(userId = "tester2@example.com")
    fun testUpdateEventWhenOtherUser() {
        val event = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        val request = CreateEventRequest(
                beginAt = "2019-02-02",
                occursEvery = "2 YEAR",
                eventTime = "10:00:00",
                personInfo = "tester2",
                note = "Testing2 note",
                useReminder = true
        )

        mockMvc.perform(
                put("/api/v1/events/{eventId}", event.id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(status().isBadRequest)

        val results = this.eventRepository.findAll()

        assertThat(results).hasSize(1)
        assertThat(results[0].userId).isEqualTo("tester@example.com")
        assertThat(results[0].beginAt).isEqualTo(LocalDate.of(2019, 1, 1))
        assertThat(results[0].occursEvery).isEqualTo("1 YEAR")
        assertThat(results[0].eventTime).isEqualTo(LocalTime.of(9, 0, 0))
        assertThat(results[0].nextOccurrence).isAfter(LocalDateTime.now())
        assertThat(results[0].personInfo).isEqualTo("tester")
        assertThat(results[0].note).isEqualTo("dummy event")
        assertThat(results[0].useReminder).isTrue()
    }

    @DisplayName("verify event can be updated successfully")
    @Test
    @WithMockUser
    fun testUpdateEvent() {
        val event = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        val request = CreateEventRequest(
                beginAt = "2019-02-02",
                occursEvery = "2 YEAR",
                eventTime = "10:00:00",
                personInfo = "tester2",
                note = "Testing2 note",
                useReminder = true
        )

        mockMvc.perform(
                put("/api/v1/events/{eventId}", event.id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(status().isNoContent)

        val results = this.eventRepository.findAll()

        assertThat(results).hasSize(1)
        assertThat(results[0].userId).isEqualTo("tester@example.com")
        assertThat(results[0].beginAt).isEqualTo(LocalDate.of(2019, 2, 2))
        assertThat(results[0].occursEvery).isEqualTo("2 YEAR")
        assertThat(results[0].eventTime).isEqualTo(LocalTime.of(10, 0, 0))
        assertThat(results[0].nextOccurrence).isAfter(LocalDateTime.now())
        assertThat(results[0].personInfo).isEqualTo("tester2")
        assertThat(results[0].note).isEqualTo("Testing2 note")
        assertThat(results[0].useReminder).isFalse()
    }

    @DisplayName("verify event cannot be deleted when not authorized")
    @Test
    @WithMockUser(privileges = [Privileges.ROLE_MONITORING])
    fun testDeleteEventWhenNotAuthorized() {
        mockMvc.perform(delete("/api/v1/events/{id}", 1))
                .andExpect(status().isForbidden)
    }

    @DisplayName("verify event cannot be deleted when not logged in")
    @Test
    fun testDeleteEventWhenNotLoggedIn() {
        mockMvc.perform(delete("/api/v1/events/{id}", 1))
                .andExpect(status().isForbidden)
    }

    @DisplayName("verify event cannot be deleted when logged in as some other user")
    @Test
    @WithMockUser(userId = "tester2@example.com")
    fun testDeleteEventWhenOtherUser() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        mockMvc.perform(delete("/api/v1/events/{id}", eventListing.id))
                .andExpect(status().isNoContent)

        val events = eventRepository.findAll()
        assertThat(events).hasSize(1)
    }

    @DisplayName("verify event can be deleted ever when there is no event")
    @Test
    @WithMockUser
    fun testDeleteEventWhenNoEvent() {
        mockMvc.perform(delete("/api/v1/events/{id}", 1))
                .andExpect(status().isNoContent)
    }

    @DisplayName("verify event can be deleted")
    @Test
    @WithMockUser
    fun testDeleteEvent() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        mockMvc.perform(delete("/api/v1/events/{id}", eventListing.id))
                .andExpect(status().isNoContent)

        val events = eventRepository.findAll()
        assertThat(events).hasSize(0)
    }

    @DisplayName("verify user cannot fetch list of events when not logged in")
    @Test
    fun testListEventsWhenNotLoggedIn() {
        mockMvc.perform(get("/api/v1/events"))
                .andExpect(status().isForbidden)
    }

    @DisplayName("verify user cannot fetch single event when not logged")
    @Test
    fun testGetEventWhenNotLoggedIn() {
        mockMvc.perform(
                get("/api/v1/events/{id}", 2))
                .andExpect(status().isForbidden)
    }

    @DisplayName("verify user cannot create a new event when not authorized")
    @Test
    @WithMockUser(privileges = [Privileges.ROLE_MONITORING])
    fun testCreateEventWhenNotAuthorized() {
        val request = createDefaultCreateEventRequest()

        mockMvc.perform(
                post("/api/v1/events")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @DisplayName("verify user cannot create a new event when not logged in")
    @Test
    fun testCreateEventWhenNotLoggedIn() {
        val request = createDefaultCreateEventRequest()

        mockMvc.perform(
                post("/api/v1/events")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(status().isForbidden)
                .andReturn()
    }

    @DisplayName("verify user cannot create a new event when request is incorrect")
    @Test
    @WithMockUser
    fun testCreateEventWhenBadRequest() {
        val request = CreateEventRequest(
                beginAt = "2019",
                occursEvery = "1",
                eventTime = "09",
                personInfo = "",
                note = "",
                useReminder = true
        )

        val result = mockMvc.perform(
                post("/api/v1/events")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(status().isBadRequest)
                .andReturn()

        val errorResponse = fromJson(result, ErrorResponse::class.java)

        assertThat(errorResponse).isNotNull
        assertThat(errorResponse.statusDescription).isEqualTo("validation error occurred")
        assertThat(errorResponse.data).hasSize(5)
        assertThat(errorResponse.data[0].key).isEqualTo("krjura.DateConstraint")
        assertThat(errorResponse.data[0].message).isEqualTo("krjura.DateConstraint")
        assertThat(errorResponse.data[0].attributeName).isEqualTo("beginAt")
        assertThat(errorResponse.data[0].attributeValues).hasSize(1)
        assertThat(errorResponse.data[0].attributeValues[0]).isEqualTo("2019")
        assertThat(errorResponse.data[1].key).isEqualTo("krjura.TimeConstraint")
        assertThat(errorResponse.data[1].message).isEqualTo("krjura.TimeConstraint")
        assertThat(errorResponse.data[1].attributeName).isEqualTo("eventTime")
        assertThat(errorResponse.data[1].attributeValues).hasSize(1)
        assertThat(errorResponse.data[1].attributeValues[0]).isEqualTo("09")
        assertThat(errorResponse.data[2].key).isEqualTo("krjura.VarcharConstraint")
        assertThat(errorResponse.data[2].message).isEqualTo("krjura.VarcharConstraint")
        assertThat(errorResponse.data[2].attributeName).isEqualTo("note")
        assertThat(errorResponse.data[2].attributeValues).hasSize(1)
        assertThat(errorResponse.data[2].attributeValues[0]).isEqualTo("")
        assertThat(errorResponse.data[3].key).isEqualTo("krjura.OccursConstraint")
        assertThat(errorResponse.data[3].message).isEqualTo("krjura.OccursConstraint")
        assertThat(errorResponse.data[3].attributeName).isEqualTo("occursEvery")
        assertThat(errorResponse.data[3].attributeValues).hasSize(1)
        assertThat(errorResponse.data[3].attributeValues[0]).isEqualTo("1")
        assertThat(errorResponse.data[4].key).isEqualTo("krjura.VarcharConstraint")
        assertThat(errorResponse.data[4].message).isEqualTo("krjura.VarcharConstraint")
        assertThat(errorResponse.data[4].attributeName).isEqualTo("personInfo")
        assertThat(errorResponse.data[4].attributeValues).hasSize(1)
        assertThat(errorResponse.data[4].attributeValues[0]).isEqualTo("")
    }

    @DisplayName("verify user can create new event")
    @Test
    @WithMockUser
    fun testCreateEvent() {
        val request = createDefaultCreateEventRequest()

        mockMvc.perform(
                post("/api/v1/events")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(status().isNoContent)
                .andReturn()

        val results = this.eventRepository.findAll()

        assertThat(results).hasSize(1)
        assertThat(results[0].userId).isEqualTo("tester@example.com")
        assertThat(results[0].beginAt).isEqualTo(LocalDate.of(2019, 1, 1))
        assertThat(results[0].occursEvery).isEqualTo("1 YEAR")
        assertThat(results[0].eventTime).isEqualTo(LocalTime.of(9, 0, 0))
        assertThat(results[0].nextOccurrence).isAfter(LocalDateTime.now())
        assertThat(results[0].personInfo).isEqualTo("tester")
        assertThat(results[0].note).isEqualTo("Testing note")
        assertThat(results[0].useReminder).isTrue()
    }

    private fun createDefaultCreateEventRequest(): CreateEventRequest {
        return CreateEventRequest(
                beginAt = "2019-01-01",
                occursEvery = "1 YEAR",
                eventTime = "09:00:00",
                personInfo = "tester",
                note = "Testing note",
                useReminder = true
        )
    }
}