package org.krjura.projects.events.integration.controllers

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.krjura.projects.events.controllers.pojo.CreateEventReminderRequest
import org.krjura.projects.events.enums.NotificationRequestState
import org.krjura.projects.events.enums.NotificationType
import org.krjura.projects.events.enums.Privileges
import org.krjura.projects.events.ex.response.ErrorResponse
import org.krjura.projects.events.model.ModelTestUtils
import org.krjura.projects.events.repository.ReminderRepository
import org.krjura.projects.events.repository.EventRepository
import org.krjura.projects.events.repository.NotificationRequestRepository
import org.krjura.projects.events.support.WebTestBase
import org.krjura.projects.events.support.WithMockUser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.LocalDateTime
import java.time.LocalTime

@DisplayName("verify provisioning of event reminders")
class EventReminderControllerTest : WebTestBase() {

    companion object {
        private const val defaultUserId = "tester@example.com"
    }

    @Autowired
    lateinit var eventRepository: EventRepository

    @Autowired
    lateinit var reminderRepository: ReminderRepository

    @Autowired
    lateinit var notificationRepository: NotificationRequestRepository

    @DisplayName("verify user cannot delete reminder when not authorized")
    @Test
    @WithMockUser(privileges = [Privileges.ROLE_MONITORING])
    fun testDeleteReminderWhenNotAuthenticated() {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(
                        "/api/v1/events/{eventId}/reminders/{reminderId}", 1, 1))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }

    @DisplayName("verify user cannot delete reminder when not logged in")
    @Test
    fun testDeleteReminderWhenNotLogged() {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(
                        "/api/v1/events/{eventId}/reminders/{reminderId}", 1, 1))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }

    @DisplayName("verify user can delete reminder when one does not exist")
    @Test
    @WithMockUser
    fun testDeleteReminderWhenNoReminders() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        mockMvc.perform(
                MockMvcRequestBuilders.delete(
                        "/api/v1/events/{eventId}/reminders/{reminderId}", eventListing.id, 1))
                .andExpect(MockMvcResultMatchers.status().isNoContent)

        val reminders = this.reminderRepository.findAll()

        assertThat(reminders).hasSize(0)
    }

    @DisplayName("verify user cannot delete reminder belonging to other user")
    @Test
    @WithMockUser(userId = "tester2@example.com")
    fun testDeleteReminderWhenOtherUser() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)
        val reminder = ModelTestUtils.defaultEventReminder(reminderRepository, defaultUserId, eventListing)

        mockMvc.perform(
                MockMvcRequestBuilders.delete(
                        "/api/v1/events/{eventId}/reminders/{reminderId}", eventListing.id, reminder.id))
                .andExpect(MockMvcResultMatchers.status().isNoContent)

        val reminders = this.reminderRepository.findAll()

        assertThat(reminders).hasSize(1)
    }

    @DisplayName("verify user can delete reminder")
    @Test
    @WithMockUser
    fun testDeleteReminder() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)
        val reminder = ModelTestUtils.defaultEventReminder(reminderRepository, defaultUserId, eventListing)
        ModelTestUtils.defaultNotification(reminder, this.notificationRepository)

        mockMvc.perform(
                MockMvcRequestBuilders.delete(
                        "/api/v1/events/{eventId}/reminders/{reminderId}", eventListing.id, reminder.id))
                .andExpect(MockMvcResultMatchers.status().isNoContent)

        val reminders = this.reminderRepository.findAll()

        assertThat(reminders).hasSize(0)

        val notificationFromDb = this.notificationRepository.findByReminderId(reminder.id!!)
        assertThat(notificationFromDb.isPresent).isFalse()
    }

    @DisplayName("verify user cannot fetch list of reminders when not logged")
    @Test
    fun testFetchEventRemindersWhenNotLoggedIn() {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/v1/events/{eventId}/reminders", 1))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }

    @DisplayName("verify user cannot create reminder with invalid data")
    @Test
    @WithMockUser
    fun testCreateEventReminderWhenBadRequest() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        val request = CreateEventReminderRequest(
                eventTime = "09",
                occursBefore = "1"
        )

        val result = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/v1/events/{eventId}/reminders", eventListing.id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
                .andReturn()

        val errorResponse = fromJson(result, ErrorResponse::class.java)

        assertThat(errorResponse).isNotNull
        assertThat(errorResponse.statusDescription).isEqualTo("validation error occurred")
        assertThat(errorResponse.data).hasSize(2)
        assertThat(errorResponse.data[0].key).isEqualTo("krjura.TimeConstraint")
        assertThat(errorResponse.data[0].message).isEqualTo("krjura.TimeConstraint")
        assertThat(errorResponse.data[0].attributeName).isEqualTo("eventTime")
        assertThat(errorResponse.data[0].attributeValues).hasSize(1)
        assertThat(errorResponse.data[0].attributeValues[0]).isEqualTo("09")
        assertThat(errorResponse.data[1].key).isEqualTo("krjura.OccursConstraint")
        assertThat(errorResponse.data[1].message).isEqualTo("krjura.OccursConstraint")
        assertThat(errorResponse.data[1].attributeName).isEqualTo("occursBefore")
        assertThat(errorResponse.data[1].attributeValues).hasSize(1)
        assertThat(errorResponse.data[1].attributeValues[0]).isEqualTo("1")
    }

    @DisplayName("verify user cannot create reminder with not authorized")
    @Test
    @WithMockUser(privileges = [Privileges.ROLE_MONITORING])
    fun testCreateEventReminderWhenNotAuthorized() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        val request = CreateEventReminderRequest(
                eventTime = "09:00:00",
                occursBefore = "1 DAY"
        )

        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/v1/events/{eventId}/reminders", eventListing.id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }

    @DisplayName("verify user cannot create reminder with not logged")
    @Test
    fun testCreateEventReminderWhenNotLoggedIn() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        val request = CreateEventReminderRequest(
                eventTime = "09:00:00",
                occursBefore = "1 DAY"
        )

        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/v1/events/{eventId}/reminders", eventListing.id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }

    @DisplayName("verify user cannot create reminder for event belonging to other user")
    @Test
    @WithMockUser
    fun testCreateEventReminderWhenDifferentUser() {
        val request = CreateEventReminderRequest(
                eventTime = "09:00:00",
                occursBefore = "1 DAY"
        )

        val result = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/v1/events/{eventId}/reminders", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
                .andReturn()

        val errorResponse = fromJson(result, ErrorResponse::class.java)

        assertThat(errorResponse).isNotNull
        assertThat(errorResponse.statusDescription).isNotNull()
        assertThat(errorResponse.data).hasSize(1)
        assertThat(errorResponse.data[0].key).isEqualTo("krjura.ex.EventProcessingException")
        assertThat(errorResponse.data[0].message).isEqualTo("krjura.ex.EventProcessingException")
        assertThat(errorResponse.data[0].attributeName).isEqualTo("")
        assertThat(errorResponse.data[0].attributeValues).hasSize(0)
    }

    @DisplayName("verify user cannot create reminder when event does not exist")
    @Test
    @WithMockUser
    fun testCreateEventReminderWhenNoEvent() {
        val request = CreateEventReminderRequest(
                eventTime = "09:00:00",
                occursBefore = "1 DAY"
        )

        val result = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/v1/events/{eventId}/reminders", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
                .andReturn()

        val errorResponse = fromJson(result, ErrorResponse::class.java)

        assertThat(errorResponse).isNotNull
        assertThat(errorResponse.statusDescription).isNotNull()
        assertThat(errorResponse.data).hasSize(1)
        assertThat(errorResponse.data[0].key).isEqualTo("krjura.ex.EventProcessingException")
        assertThat(errorResponse.data[0].message).isEqualTo("krjura.ex.EventProcessingException")
        assertThat(errorResponse.data[0].attributeName).isEqualTo("")
        assertThat(errorResponse.data[0].attributeValues).hasSize(0)
    }

    @DisplayName("verify user can create event reminder")
    @Test
    @WithMockUser
    fun testCreateEventReminder() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        val request = CreateEventReminderRequest(
                eventTime = "09:00:00",
                occursBefore = "1 DAY"
        )

        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/v1/events/{eventId}/reminders", eventListing.id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andExpect(MockMvcResultMatchers.status().isNoContent)

        val reminders = this.reminderRepository.findAll()

        assertThat(reminders).hasSize(1)
        assertThat(reminders[0].userId).isEqualTo(defaultUserId)
        assertThat(reminders[0].eventId).isEqualTo(eventListing.id)
        assertThat(reminders[0].eventTime).isEqualTo(LocalTime.of(9, 0, 0))
        assertThat(reminders[0].occursBefore).isEqualTo("1 DAY")

        val notification = this.notificationRepository.findByReminderId(reminders[0].id!!)

        assertThat(notification.isPresent).isTrue()
        assertThat(notification.get().attempts).isEqualTo(0)
        assertThat(notification.get().reminderId).isEqualTo(reminders[0].id)
        assertThat(notification.get().state).isEqualTo(NotificationRequestState.OPEN)
        assertThat(notification.get().type).isEqualTo(NotificationType.REMINDER)
        assertThat(notification.get().executeOn).isEqualTo(LocalDateTime.of(2019, 12, 31, 9, 0, 0, 0))
    }
}