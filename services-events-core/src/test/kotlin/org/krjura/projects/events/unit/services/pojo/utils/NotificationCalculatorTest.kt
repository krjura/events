package org.krjura.projects.events.unit.services.pojo.utils

import org.junit.jupiter.api.DisplayName
import org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test
import org.krjura.projects.events.services.pojo.utils.NotificationCalculator
import java.time.LocalDateTime

@DisplayName("verify notification execute on calculator")
class NotificationCalculatorTest {

    @DisplayName("check calculation")
    @Test
    fun calculateTest() {
        val eventTime = LocalDateTime.of(2019, 1, 1, 9, 0, 0, 0)

        assertThat(NotificationCalculator.calculate(eventTime, "1 DAY"))
                .isEqualTo(LocalDateTime.of(2018, 12, 31, 9, 0, 0, 0))

        assertThat(NotificationCalculator.calculate(eventTime, "1 WEEK"))
                .isEqualTo(LocalDateTime.of(2018, 12, 25, 9, 0, 0, 0))

        assertThat(NotificationCalculator.calculate(eventTime, "1 MONTH"))
                .isEqualTo(LocalDateTime.of(2018, 12, 1, 9, 0, 0, 0))

        assertThat(NotificationCalculator.calculate(eventTime, "1 YEAR"))
                .isEqualTo(LocalDateTime.of(2018, 1, 1, 9, 0, 0, 0))

        assertThat(NotificationCalculator.calculate(eventTime, "5 DAY"))
                .isEqualTo(LocalDateTime.of(2018, 12, 27, 9, 0, 0, 0))

        assertThat(NotificationCalculator.calculate(eventTime, "5 WEEK"))
                .isEqualTo(LocalDateTime.of(2018, 11, 27, 9, 0, 0, 0))

        assertThat(NotificationCalculator.calculate(eventTime, "5 MONTH"))
                .isEqualTo(LocalDateTime.of(2018, 8, 1, 9, 0, 0, 0))

        assertThat(NotificationCalculator.calculate(eventTime, "5 YEAR"))
                .isEqualTo(LocalDateTime.of(2014, 1, 1, 9, 0, 0, 0))
    }
}