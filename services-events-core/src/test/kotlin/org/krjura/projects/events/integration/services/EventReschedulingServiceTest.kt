package org.krjura.projects.events.integration.services

import org.junit.jupiter.api.Test
import org.krjura.projects.events.model.Event
import org.krjura.projects.events.repository.EventRepository
import org.krjura.projects.events.services.EventReschedulingService
import org.krjura.projects.events.support.TestBase
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.krjura.projects.events.enums.NotificationRequestState
import org.krjura.projects.events.enums.NotificationType
import org.krjura.projects.events.model.ModelTestUtils
import org.krjura.projects.events.repository.ReminderRepository
import org.krjura.projects.events.repository.NotificationRequestRepository

@DisplayName("verify event can be rescheduled")
class EventReschedulingServiceTest : TestBase() {

    companion object {
        const val defaultUserId = "tester@example.com"
    }

    @Autowired
    lateinit var eventReschedulingService: EventReschedulingService

    @Autowired
    lateinit var eventRepository: EventRepository

    @Autowired
    lateinit var reminderRepositoryy: ReminderRepository

    @Autowired
    lateinit var notificationRepository: NotificationRequestRepository;

    @Test
    @DisplayName("basic")
    fun testRescheduling() {
        val event = createDefaultExpiredEvent()

        eventReschedulingService.checkSafely()

        val events = this.eventRepository.findAll()
        assertThat(events).hasSize(1)
        assertThat(events[0].userId).isEqualTo(event.userId)
        assertThat(events[0].beginAt).isEqualTo(event.beginAt)
        assertThat(events[0].occursEvery).isEqualTo(event.occursEvery)
        assertThat(events[0].eventTime).isEqualTo(event.eventTime)
        assertThat(events[0].nextOccurrence).isAfter(LocalDateTime.now())
        assertThat(events[0].personInfo).isEqualTo(event.personInfo)
        assertThat(events[0].note).isEqualTo(event.note)
        assertThat(events[0].useReminder).isEqualTo(event.useReminder)
    }

    @Test
    @DisplayName("with reminders")
    fun testReschedulingWithReminders() {
        val event = createDefaultExpiredEvent()
        val reminder = ModelTestUtils.defaultEventReminder(this.reminderRepositoryy, defaultUserId, event)

        eventReschedulingService.checkSafely()

        val events = this.eventRepository.findAll()
        assertThat(events).hasSize(1)
        assertThat(events[0].userId).isEqualTo(event.userId)
        assertThat(events[0].beginAt).isEqualTo(event.beginAt)
        assertThat(events[0].occursEvery).isEqualTo(event.occursEvery)
        assertThat(events[0].eventTime).isEqualTo(event.eventTime)
        assertThat(events[0].nextOccurrence).isAfter(LocalDateTime.now())
        assertThat(events[0].personInfo).isEqualTo(event.personInfo)
        assertThat(events[0].note).isEqualTo(event.note)
        assertThat(events[0].useReminder).isEqualTo(event.useReminder)

        val notifications = this.notificationRepository.findAll()
        assertThat(notifications).hasSize(1)
        assertThat(notifications[0].reminderId).isEqualTo(reminder.id!!)
        assertThat(notifications[0].type).isEqualTo(NotificationType.REMINDER)
        assertThat(notifications[0].state).isEqualTo(NotificationRequestState.OPEN)
        assertThat(notifications[0].attempts).isEqualTo(0)
        assertThat(notifications[0].executeOn).isAfter(LocalDateTime.now())
    }

    private fun createDefaultExpiredEvent(): Event {
        val event = Event(
                null,
                defaultUserId,
                LocalDate.of(2019, 1, 1),
                "1 MONTH",
                LocalTime.of(9, 0, 0),
                LocalDateTime.of(2019, 2, 1, 9, 0, 0),
                "tester",
                "dummy event",
                true
        )

        return this.eventRepository.save(event)
    }
}