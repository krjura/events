package org.krjura.projects.events.support

import org.krjura.projects.events.enums.Privileges
import org.springframework.security.test.context.support.WithSecurityContext

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
@WithSecurityContext(factory = WithMockContextFactory::class)
annotation class WithMockUser(
        val userId: String = "tester@example.com",
        val privileges: Array<Privileges> = [Privileges.ROLE_USER]
)