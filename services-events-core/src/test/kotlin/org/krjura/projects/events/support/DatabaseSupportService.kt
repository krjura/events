package org.krjura.projects.events.support

import org.krjura.projects.events.repository.ReminderRepository
import org.krjura.projects.events.repository.EventRepository
import org.krjura.projects.events.repository.NotificationRequestRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DatabaseSupportService {

    @Autowired
    lateinit var eventRepository: EventRepository;

    @Autowired
    lateinit var reminderRepository: ReminderRepository

    @Autowired
    lateinit var notificationRepository: NotificationRequestRepository

    fun deleteAll() {
        this.reminderRepository.deleteAll()
        this.eventRepository.deleteAll()
        this.notificationRepository.deleteAll()
    }
}