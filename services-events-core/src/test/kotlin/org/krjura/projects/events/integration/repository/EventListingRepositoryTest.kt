package org.krjura.projects.events.integration.repository

import org.krjura.projects.events.model.Event
import org.krjura.projects.events.support.TestBase
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDate
import java.time.LocalTime
import java.util.Optional

import org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.krjura.projects.events.model.ModelTestUtils
import org.krjura.projects.events.repository.EventRepository

@DisplayName("verify event repository")
class EventListingRepositoryTest: TestBase() {

    companion object {
        const val defaultUserId = "tester@example.com"
    }

    @Autowired
    lateinit var eventRepository: EventRepository

    @DisplayName("verify event repository works as expected")
    @Test
    fun testEventListingRepository() {
        val eventListing = ModelTestUtils.defaultEvent(eventRepository, defaultUserId)

        val fromDb: Optional<Event> = eventRepository.findById(eventListing.id!!)

        assertThat(fromDb).isPresent
        assertThat(fromDb.get().beginAt).isEqualTo(LocalDate.of(2019, 1, 1))
        assertThat(fromDb.get().occursEvery).isEqualTo("1 YEAR")
        assertThat(fromDb.get().eventTime).isEqualTo(LocalTime.of(9, 0, 0))
        assertThat(fromDb.get().personInfo).isEqualTo("tester")
        assertThat(fromDb.get().note).isEqualTo("dummy event")
        assertThat(fromDb.get().useReminder).isTrue()
    }
}