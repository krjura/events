import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './pages/events/dashboard/dashboard.component';
import { EventsRootComponent } from './pages/events/events-root.component';
import { CreateEventComponent } from './pages/events/create-event/create-event.component';
import { UserGuardModel } from './shared/guards/user.guard.model';
import { AuthErrorComponent } from './pages/default/auth-error/auth-error.component';
import { LoginErrorComponent } from './pages/default/login-error/login-error.component';

const routes: Routes = [
  {
    path: 'events',
    component: EventsRootComponent,
    canActivate: [UserGuardModel],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'create-event',
        component: CreateEventComponent,
      },
      {
        path: 'create-event/:eventId',
        component: CreateEventComponent,
      }
    ]
  },
  {
    path: 'auth-error',
    component: AuthErrorComponent
  },
  {
    path: 'login-error',
    component: LoginErrorComponent
  },
  // otherwise redirect to home
  { path: '**', redirectTo: '/events/dashboard' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
