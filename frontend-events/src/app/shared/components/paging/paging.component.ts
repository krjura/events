import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import { PageContextModel } from '@shared/model/page-context.model';

@Component({
  selector: 'app-paging',
  templateUrl: './paging.component.html',
  styleUrls: ['./paging.component.scss']
})
export class PagingComponent implements OnInit {

  @Output() pageChange = new EventEmitter<PageContextModel>();

  @Input() pageSize = 10;

  currentPageNumber = 0;

  hasPrevious = false;
  hasNext = true;

  showFirst = false;

  constructor() {

  }

  ngOnInit() {

  }

  process() {
    this.showFirst = this.currentPageNumber > 2;
    this.hasPrevious = this.currentPageNumber > 0;
  }

  goToPreviousPage() {
    if (this.currentPageNumber <= 0) {
      return;
    }

    this.currentPageNumber = this.currentPageNumber - 1;
    this.pageChange.next(new PageContextModel(this.currentPageNumber, this.pageSize));
    this.process();
  }

  goToNextPage() {
    this.currentPageNumber = this.currentPageNumber + 1;
    this.pageChange.next(new PageContextModel(this.currentPageNumber, this.pageSize));
    this.process();
  }

  goToFirstPage() {
    this.currentPageNumber = 0;
    this.pageChange.next(new PageContextModel(this.currentPageNumber, this.pageSize));
    this.process();
  }
}
