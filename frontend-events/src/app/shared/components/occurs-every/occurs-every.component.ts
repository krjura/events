import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

const defaultValue = '1';

@Component({
  selector: 'app-occurs-every',
  templateUrl: './occurs-every.component.html',
  styleUrls: ['./occurs-every.component.scss']
})
export class OccursEveryComponent implements OnInit, OnDestroy {

  @Input() occursEveryFormGroup: FormGroup;
  @Input() occursEveryFormControlName: string;

  value: string = defaultValue;
  timeUnit = 'DAY';

  valueChangeSubscription: Subscription = null;

  constructor() {

  }

  ngOnInit() {
    this.onChange();
    this.listenToFormChanges();
  }

  private listenToFormChanges() {
    this.valueChangeSubscription = this
      .occursEveryFormGroup
      .controls[this.occursEveryFormControlName]
      .valueChanges
      .subscribe(value => {
          const parts: string[] = value.toString().split(' ');

          this.value = parts[0];
          this.timeUnit = parts[1];
      });
  }

  ngOnDestroy(): void {
    if (this.valueChangeSubscription != null) {
      this.valueChangeSubscription.unsubscribe();
    }
  }

  onChange() {
    const formatted = '' + this.value + ' ' + this.timeUnit;
    this.occursEveryFormGroup.controls[this.occursEveryFormControlName].patchValue(formatted);
  }

  numberUp() {
    const converted = parseInt(this.value, 10);

    this.value = '' + (converted + 1);

    this.onChange();
  }

  numberDown() {
    const converted = parseInt(this.value, 10);

    if (converted <= 1) {
      return;
    }

    this.value = '' + (converted - 1);

    this.onChange();
  }

  timeUnitChanged(value: string) {
    this.timeUnit = value;
    this.onChange();
  }

  valueChanged() {
    const converted = parseInt(this.value, 10);

    if (isNaN(converted)) {
      this.value = defaultValue;
    }

    this.onChange();
  }
}
