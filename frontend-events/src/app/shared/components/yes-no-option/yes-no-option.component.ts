import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';

import { ChoiceDataModel } from './choice-data.model';

@Component({
  selector: 'app-yes-no-option',
  templateUrl: './yes-no-option.component.html',
  styleUrls: ['./yes-no-option.component.scss']
})
export class YesNoOptionComponent implements OnDestroy {

  @Output() selection = new EventEmitter<ChoiceDataModel>();

  @Input() value: object;

  constructor() {

  }

  ngOnDestroy(): void {
    this.selection.unsubscribe();
  }

  onYes() {
    this.selection.emit({
      choice: true,
      value: this.value
    });
  }

  onNo() {
    this.selection.emit({
      choice: false,
      value: this.value
    });
  }
}
