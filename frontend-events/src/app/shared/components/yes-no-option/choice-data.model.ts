export class ChoiceDataModel {

  constructor(public choice: boolean, public value: any) {
    this.choice = choice;
    this.value = value;
  }
}
