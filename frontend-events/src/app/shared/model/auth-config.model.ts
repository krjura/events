export class AuthConfigModel {
  authenticated: boolean;
  username: string;
  privileges: string[];
}
