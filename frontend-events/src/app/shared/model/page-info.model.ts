export class PageInfoModel {

  constructor(
    public page: number = 0, public size: number = 0, public hasPrevious: boolean, public hasNext: boolean) {

  }

  static ofFirst(): PageInfoModel {
    return new PageInfoModel(0, 50, false, false);
  }

  static of(pageInfo: PageInfoModel, data: object[]) {
    return new PageInfoModel(
      pageInfo.size,
      pageInfo.page,
      pageInfo.page > 0,
      pageInfo.size === data.length
    );
  }
}
