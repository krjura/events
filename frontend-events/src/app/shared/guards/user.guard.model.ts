import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { AuthenticationService } from '@shared/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UserGuardModel implements CanActivate {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    if (this.authenticationService.isUser()) {
      return true;
    }

    if (this.authenticationService.isAuthenticated()) {
      this.authenticationService.reportAuthErrorUrl(state.url);
      this.router.navigate(['/auth-error']);
    } else {
      this.router.navigate(['/login-error']);
    }

    return false;
  }
}
