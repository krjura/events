export class DateTimeUtils {

  public static parseDate(date: string) {
    const parts = date.split('-');

    const year = parseInt(parts[0], 10);
    const month = parseInt(parts[1], 10) - 1;
    const day = parseInt(parts[2], 10);

    return new Date(year, month, day, 0, 0, 0, 0);
  }

  public static formatDate(date: Date) {

    const year = date.getFullYear().toString(10);
    const month = DateTimeUtils.leftPad( (date.getMonth() + 1).toString(10));
    const day = DateTimeUtils.leftPad(date.getDate().toString(10));

    return year + '-' + month + '-' + day;
  }

  public static parseTime(date: string) {
    const parts = date.split(':');

    const hour = parseInt(parts[0], 10);
    const minute = parseInt(parts[1], 10);
    const second = parseInt(parts[2], 10);

    const current = new Date();
    current.setHours(hour, minute, second, 0);

    return current;
  }

  public static formatTime(date: Date) {
    const hour = DateTimeUtils.leftPad(date.getHours().toString(10));
    const minute = DateTimeUtils.leftPad(date.getMinutes().toString(10));
    const second = DateTimeUtils.leftPad(date.getSeconds().toString(10));

    return hour + ':' + minute + ':' + second;
  }

  public static leftPad(val: string) {

    const correctionLength = 2 - val.length;

    if (correctionLength <= 0 ) {
      return val;
    }

    let fixedData = val;

    for (let i = 0; i < correctionLength; i++) {
      fixedData = '0' + fixedData;
    }

    return fixedData;
  }
}
