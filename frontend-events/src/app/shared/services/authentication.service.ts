import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AuthConfigModel } from '@shared/model/auth-config.model';

@Injectable()
export class AuthenticationService {

  private authConfig: AuthConfigModel;

  private authenticated = false;
  private flagIsUser = false;

  private lastAuthErrorUrl: string = null;

  constructor(
    private http: HttpClient) {

  }

  loadConfig(): Promise<boolean> {

    return new Promise(( resolve, reject) => {
      this
        .http.get<AuthConfigModel>('/api/v1/auth/config')
        .subscribe(response => {

          this.authConfig = response;
          this.processAuthConfig();

          resolve(true);
        }, error => {

          // const current = window.location;
          // window.location.href = current.protocol + "//" + current.hostname + ":" + current.port + "/login";

          resolve(false);
        });
    });
  }

  private processAuthConfig() {
    this.authenticated = this.authConfig.authenticated;

    for (const privilege of this.authConfig.privileges) {

      if (privilege === 'ROLE_USER') {
        this.flagIsUser = true;
      }
    }
  }

  isUser(): boolean {
    return this.flagIsUser;
  }

  isAuthenticated(): boolean {
    return this.authenticated;
  }

  reportAuthErrorUrl(url: string) {
    this.lastAuthErrorUrl = url;
  }

  getLastAuthErrorUrl(): string {
    return this.lastAuthErrorUrl;
  }
}
