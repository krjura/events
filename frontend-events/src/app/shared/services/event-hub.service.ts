import { Injectable } from '@angular/core';
import { NextObserver, Subject, Subscription } from 'rxjs';

import { BasicEventModel } from '@shared/events/basic-event.model';

@Injectable({
  providedIn: 'root'
})
export class EventHubService {

  private hub: Subject<BasicEventModel> = new Subject<BasicEventModel>();

  constructor() {

  }

  register(observer: NextObserver<BasicEventModel>): Subscription {
    return this.hub.subscribe(observer);
  }

  emit(value: BasicEventModel) {
    this.hub.next(value);
  }
}
