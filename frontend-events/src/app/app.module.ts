import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';
import { ToasterModule } from 'angular2-toaster';
import { StoreModule } from '@ngrx/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticationService } from '@shared/services/authentication.service';
import { PagingComponent } from '@shared/components/paging/paging.component';
import { OccursEveryComponent } from '@shared/components/occurs-every/occurs-every.component';
import { YesNoOptionComponent } from '@shared/components/yes-no-option/yes-no-option.component';
import { DashboardComponent } from '@pevents/dashboard/dashboard.component';
import { CreateEventComponent } from '@pevents/create-event/create-event.component';
import { EventsRootComponent } from '@pevents/events-root.component';
import { AddReminderComponent } from '@pevents/create-event/add-reminder/add-reminder.component';
import { ListRemindersComponent } from '@pevents/create-event/list-reminders/list-reminders.component';
import { appReducers } from '@pevents/create-event/shared/ngrx/app.reducers.model';
import { AuthErrorComponent } from '@pdefault/auth-error/auth-error.component';
import { LoginErrorComponent } from '@pdefault/login-error/login-error.component';

export function initializeApp(appInitService: AuthenticationService) {
  return (): Promise<any> => {
    return appInitService.loadConfig();
  };
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    CreateEventComponent,
    EventsRootComponent,
    OccursEveryComponent,
    PagingComponent,
    YesNoOptionComponent,
    AddReminderComponent,
    ListRemindersComponent,
    AuthErrorComponent,
    LoginErrorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,

    NgbModule,
    Ng2FlatpickrModule,
    ToasterModule.forRoot(),
    StoreModule.forRoot(appReducers)
  ],
  providers: [
    AuthenticationService,
    { provide: APP_INITIALIZER, useFactory: initializeApp, deps: [AuthenticationService], multi: true }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {

}
