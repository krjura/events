import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { ToasterService } from 'angular2-toaster';
import { select, Store } from '@ngrx/store';

import { ChoiceDataModel } from '@shared/components/yes-no-option/choice-data.model';
import { EventHubService } from '@shared/services/event-hub.service';
import { GraphqlEventReminderModel } from '@pevents/shared/model/graphql-event-reminder.model';
import { EventsBackendService } from '@pevents/shared/service/events-backend.service';
import { AppState, EventState } from '../shared/ngrx/event.state';

@Component({
  selector: 'app-list-reminders',
  templateUrl: './list-reminders.component.html',
  styleUrls: ['./list-reminders.component.scss']
})
export class ListRemindersComponent implements OnInit, OnDestroy {

  initialized = false;
  eventId: string = null;
  eventReminders: GraphqlEventReminderModel[] = [];

  showConfirm = {};

  ngrxSubscription: Subscription = null;

  constructor(
    private eventsBackendService: EventsBackendService,
    private toasterService: ToasterService,
    private eventHubService: EventHubService,
    private store: Store<AppState>) {

  }

  ngOnInit() {
    this.initNgrxStore();
  }

  ngOnDestroy(): void {
    if (this.ngrxSubscription != null) {
      this.ngrxSubscription.unsubscribe();
    }
  }

  private initNgrxStore() {
    this.ngrxSubscription = this
      .store
      .pipe(select('event'))
      .subscribe(value => {
        const event = value as EventState;

        if (event.current != null) {
          this.eventId = event.current.id.toString();
          this.eventReminders = event.current.reminders;
          this.initialized = true;
        }
      });
  }

  onDeleteSelection(event: ChoiceDataModel) {
    const index = event.value as number;
    const reminder = this.eventReminders[index];

    if (event.choice) {
      this.deleteReminder(reminder);
    }

    this.showConfirm[index] = false;
  }

  private deleteReminder(reminder) {
    this
      .eventsBackendService
      .deleteReminder(this.eventId, reminder.id.toString())
      .subscribe(value => {
        if (value.status === 204) {
          this.toasterService.pop('success', 'Status', 'Reminder was added successful');
          this.reloadEvent();
        } else {
          this.toasterService.pop('error', 'Status', 'Failed to add reminder due to server error');
        }
      });
  }

  reloadEvent() {
    this.eventHubService.emit({ type: 'EventChanged'});
  }

  confirmDelete(index: number) {
    this.showConfirm[index] = true;
  }
}
