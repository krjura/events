import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription} from 'rxjs';

import {select, Store} from '@ngrx/store';
import { ToasterService } from 'angular2-toaster';

import { EventHubService } from '@shared/services/event-hub.service';
import { FlatpickrToolsModel } from '@pevents/shared/model/flatpickr/flatpickr-tools.model';
import { EventsBackendService } from '@pevents/shared/service/events-backend.service';
import { CreateEventReminderRequestModel } from '@pevents/shared/model/create-event-reminder-request.model';
import { AppState, EventState } from '../shared/ngrx/event.state';

@Component({
  selector: 'app-add-reminder',
  templateUrl: './add-reminder.component.html',
  styleUrls: ['./add-reminder.component.scss']
})
export class AddReminderComponent implements OnInit {

  initialized = false;
  eventId: string = null;

  form: FormGroup;
  eventTimeOptions = FlatpickrToolsModel.defaultTimeOptions;
  eventTimeFormControlValue = new Date(2019, 0, 1, 9, 0, 0, 0);

  ngrxSubscription: Subscription = null;

  constructor(
    private fb: FormBuilder,
    private eventsBackendService: EventsBackendService,
    private toasterService: ToasterService,
    private eventHubService: EventHubService,
    private store: Store<AppState>) {

  }

  ngOnInit() {
    this.setupForm();
    this.initNgrxStore();
  }

  private initNgrxStore() {
    this.ngrxSubscription = this
      .store
      .pipe(select('event'))
      .subscribe(value => {
        const event = value as EventState;

        if (event.current != null) {
          this.eventId = event.current.id.toString();
          this.initialized = true;
        }
      });
  }

  private setupForm() {
    this.form = this.fb.group({
      eventTime: ['09:00:00', [Validators.required]],
      occursEvery: ['', [Validators.required]]
    });
  }

  addReminder() {
    if (!this.form.valid) {
      return;
    }

    const request: CreateEventReminderRequestModel = {
      eventTime: this.parseEventTime(),
      occursBefore: this.form.controls.occursEvery.value
    };

    this
      .eventsBackendService
      .createReminder(this.eventId, request)
      .subscribe(value => {
        if (value.status === 204) {
          this.toasterService.pop('success', 'Status', 'Reminder was added successful');
          this.reloadEvent();
        } else {
          this.toasterService.pop('error', 'Status', 'Failed to add reminder due to server error');
        }
      });
  }

  reloadEvent() {
    this.eventHubService.emit({ type: 'EventChanged'});
  }

  private parseEventTime(): string {
    return FlatpickrToolsModel.parseTime(this.form.controls.eventTime.value);
  }
}
