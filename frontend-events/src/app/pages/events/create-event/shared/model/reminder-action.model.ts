export class ReminderActionModel {

  constructor(public action: string, public eventId: string, public reminderId: string) {
  }

}
