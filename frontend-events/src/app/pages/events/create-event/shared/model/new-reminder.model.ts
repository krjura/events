export class NewReminderModel {

  constructor(public eventId: string, public eventTime: string, public occursBefore: string) {

  }
}
