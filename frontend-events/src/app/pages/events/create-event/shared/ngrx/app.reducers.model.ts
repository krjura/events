import { ActionReducerMap } from '@ngrx/store';

import { AppState } from './event.state';
import { eventReducers } from './event.reducer';

export const appReducers: ActionReducerMap<AppState, any> = {
  event: eventReducers
};
