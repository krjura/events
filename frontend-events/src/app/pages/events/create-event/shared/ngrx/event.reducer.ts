import { EventState, initialEventState } from './event.state';
import { EventActions, EventActionTypes } from './event.actions';

export function eventReducers(state = initialEventState, action: EventActions): EventState {
  switch (action.type) {
    case EventActionTypes.Updated: {
      return {
        current: action.payload
      };
    }
    default:
      return state;
  }
}
