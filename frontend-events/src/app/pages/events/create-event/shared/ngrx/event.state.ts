import { GraphqlEventModel } from '../../../shared/model/graphql-event.model';

export interface EventState {
  current: GraphqlEventModel;
}

export const initialEventState: EventState = {
  current: null
};

export interface AppState {
  event: EventState;
}
