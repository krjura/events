import { Action } from '@ngrx/store';

import { GraphqlEventModel } from '../../../shared/model/graphql-event.model';

export enum EventActionTypes {
  Updated = 'Updated'
}

export class UpdatedEventAction implements Action {
  public readonly type = EventActionTypes.Updated;

  constructor(public payload: GraphqlEventModel) {

  }
}

export type EventActions = UpdatedEventAction;
