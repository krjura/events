import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Store } from '@ngrx/store';
import { ToasterService } from 'angular2-toaster';

import { EventHubService } from '@shared/services/event-hub.service';
import { DateTimeUtils } from '@shared/utils/date-time.utils';
import { EventsBackendService } from '@pevents/shared/service/events-backend.service';
import { CreateEventRequestModel } from '@pevents/shared/model/create-event-request.model';
import { GraphqlEventReminderModel } from '@pevents/shared/model/graphql-event-reminder.model';
import { FlatpickrToolsModel } from '@pevents/shared/model/flatpickr/flatpickr-tools.model';
import { AppState } from './shared/ngrx/event.state';
import { UpdatedEventAction } from './shared/ngrx/event.actions';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss']
})
export class CreateEventComponent implements OnInit, OnDestroy {

  eventTimeOptions = FlatpickrToolsModel.defaultTimeOptions;

  beginAtOptions = FlatpickrToolsModel.defaultDateTimeOptions;

  form: FormGroup;
  reminderForm: any;
  eventReminders: GraphqlEventReminderModel[] = [];

  initialized = false;
  eventId = null;

  // must do this since it is the only way to update value in flatpickr
  beginAtFormControlValue = new Date(2019, 0, 1, 9, 0, 0, 0);
  eventTimeFormControlValue = new Date(2019, 0, 1, 9, 0, 0, 0);

  eventHubSubscription: Subscription = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private toasterService: ToasterService,
    private eventsBackendService: EventsBackendService,
    private eventHubService: EventHubService,
    private store: Store<AppState>) {

  }

  ngOnInit() {
    this.setupForm();
    this.setupRoute();
    this.setupObserver();
  }

  ngOnDestroy(): void {
    if (this.eventHubSubscription != null) {
      this.eventHubSubscription.unsubscribe();
    }

    // clear event state
    this.store.dispatch(new UpdatedEventAction(null));
  }

  private setupObserver() {
    this.eventHubSubscription = this.eventHubService.register({
      next: value => {
        if (value.type === 'EventChanged') {
          this.reloadEvent();
        }
      }
    });
  }

  private setupRoute() {
    this.route.params.subscribe(params => {
      const eventId = parseInt(params.eventId, 10);

      if (eventId != null) {
        this.eventId = eventId;
        this.reloadEvent();
      } else {
        this.initialized = true;
      }
    });
  }

  private setupForm() {
    this.form = this.fb.group({
      eventId: [null],
      eventTime: ['09:00:00', [Validators.required]],
      beginAt: ['2019-01-01', [Validators.required]],
      occursEvery: ['', [Validators.required]],
      personInfo: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]],
      note: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]]
    });

    this.reminderForm = this.fb.group({
      occursEvery: ['', [Validators.required]]
    });
  }

  private reloadEvent() {
    this.loadEventWithGraphql(this.eventId);
  }

  private loadEventWithGraphql(eventId: number) {
    this
      .eventsBackendService
      .loadEventWithGraphql(eventId)
      .subscribe(value => {
        const event = value.body.data.eventsById;

        this.form.controls.eventId.patchValue(event.id);
        this.beginAtFormControlValue = DateTimeUtils.parseDate(event.beginAt);
        this.eventTimeFormControlValue = DateTimeUtils.parseTime(event.eventTime);
        this.form.controls.occursEvery.patchValue(event.occursEvery);
        this.form.controls.note.patchValue(event.note);
        this.form.controls.personInfo.patchValue(event.personInfo);

        this.eventReminders = event.reminders;

        this.initialized = true;
        this.eventId = event.id;

        this.store.dispatch(new UpdatedEventAction(event));
      });
  }

  saveEvent() {
    if (!this.form.valid) {
      return;
    }

    const request: CreateEventRequestModel = {
      beginAt: this.parseBeginAt(),
      eventTime: this.parseEventTime(),
      occursEvery: this.form.controls.occursEvery.value,
      personInfo: this.form.controls.personInfo.value,
      note: this.form.controls.note.value,
      useReminder: false
    };

    const eventId = this.form.controls.eventId.value;

    if (eventId != null) {
      this
        .eventsBackendService
        .updateEvent(eventId, request)
        .subscribe(value => {
          if (value.status === 204) {
            this.toasterService.pop('success', 'Status', 'Event was updated successful');
            this.reloadEvent();
          } else {
            this.toasterService.pop('error', 'Status', 'Failed to update event due to server error');
          }

          this.redirectToDashboard();
        });
    } else {
      this
        .eventsBackendService
        .createEvent(request)
        .subscribe(value => {
          if (value.status === 204) {
            this.toasterService.pop('success', 'Status', 'Event was added successful');
          } else {
            this.toasterService.pop('error', 'Status', 'Failed to add event due to server error');
          }

          this.redirectToDashboard();
        });
    }

  }

  private parseBeginAt(): string {
    return FlatpickrToolsModel.parseDateTime(this.form.controls.beginAt.value);
  }

  private parseEventTime(): string {
    return FlatpickrToolsModel.parseTime(this.form.controls.eventTime.value);
  }

  private redirectToDashboard() {
    this.router.navigate(['/events/dashboard']);
  }
}
