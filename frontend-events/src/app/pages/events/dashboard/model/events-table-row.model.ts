export class EventsTableRowModel {
  id: number;
  beginAt: string;
  occursEvery: string;
  eventTime: string;
  personInfo: string;
  note: string;
  useReminder: boolean;

  showNote: boolean;
  showConfirm: boolean;
}
