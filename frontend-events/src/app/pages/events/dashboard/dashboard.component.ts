import { Component, OnDestroy, OnInit } from '@angular/core';

import { ToasterService } from 'angular2-toaster';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { PageContextModel } from '@shared/model/page-context.model';
import { ChoiceDataModel } from '@shared/components/yes-no-option/choice-data.model';
import { EventsBackendService } from '@pevents/shared/service/events-backend.service';
import { GraphqlEventModel } from '@pevents/shared/model/graphql-event.model';
import { EventsTableRowModel } from './model/events-table-row.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  initialized = false;
  hasData = false;
  data: EventsTableRowModel[];

  searchForm: FormGroup;
  subscription: Subscription;

  currentPageContext = new PageContextModel(0, 10);

  static mapToRowData(value: GraphqlEventModel) {
    const model = new EventsTableRowModel();
    model.id = value.id;
    model.beginAt = value.beginAt;
    model.occursEvery = value.occursEvery;
    model.eventTime = value.eventTime;
    model.personInfo = value.personInfo;
    model.note = value.note;
    model.useReminder = value.useReminder;
    model.showNote = false;
    model.showConfirm = false;

    return model;
  }

  constructor(
    private fb: FormBuilder,
    private eventsBackendService: EventsBackendService,
    private toasterService: ToasterService) {

  }

  ngOnInit() {
    this.setupForm();
    this.reloadPage();
  }

  ngOnDestroy() {
    if(this.subscription != null) {
      this.subscription.unsubscribe();
    }
  }

  private setupForm() {
    this.searchForm = this.fb.group({
      searchText: ['']
    });

    this.subscription = this
      .searchForm
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(() => {
        this.reloadPage();
      })
  }

  reloadPage() {
    this.listEventsWithGraphQL(this.currentPageContext);
  }

  onSearch() {
    this.reloadPage();
  }

  listEventsWithGraphQL(pageContext: PageContextModel) {

    const searchText = this.searchForm.controls.searchText.value;

    this
      .eventsBackendService
      .listEventsWithGraphQL(searchText, pageContext)
      .subscribe( result => {
        this.data = result.body.data.eventsByUser.map(value => DashboardComponent.mapToRowData(value));

        this.initialized = true;
        this.hasData = this.data.length > 0;

        this.currentPageContext = pageContext;
      });
  }

  deleteEvent(eventId: number) {
    this
      .eventsBackendService
      .deleteEvent(eventId)
      .subscribe( result => {
        if (result.status === 204) {
          this.toasterService.pop('success', 'Status', 'Event was deleted');
        } else {
          this.toasterService.pop('error', 'Status', 'Failed to delete event due to server error');
        }

        this.reloadPage();
      });
  }

  toggleRowDetails(i: number) {
    this.data[i].showNote = !this.data[i].showNote;
  }

  onPageChange(pageContext: PageContextModel) {
    this.listEventsWithGraphQL(pageContext);
  }

  deleteRow(i: number) {
    this.data[i].showConfirm = true;
  }

  onDeleteSelection(result: ChoiceDataModel) {
    const index = result.value as number;

    this.data[index].showConfirm = false;

    if (result.choice) {
      this.deleteEvent(this.data[index].id);
    }
  }
}
