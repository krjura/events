import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { PageContextModel } from '@shared/model/page-context.model';
import { CreateEventRequestModel } from '../model/create-event-request.model';
import { GraphqlListEventsModel } from '../model/graphql-list-events.model';
import { GraphqlEventByIdModel } from '../model/graphql-event-by-id.model';
import { CreateEventReminderRequestModel } from '../model/create-event-reminder-request.model';

@Injectable({
  providedIn: 'root'
})
export class EventsBackendService {

  constructor(
    private http: HttpClient) {

  }

  createEvent(request: CreateEventRequestModel): Observable<HttpResponse<object>> {

    const url = '/api/v1/events';
    const jsonHeaders = new HttpHeaders().append('Content-Type', 'application/json');

    return this
      .http
      .post(
        url,
        request,
        {
          headers: jsonHeaders,
          observe: 'response',
          withCredentials: true
        }
      )
      .pipe(catchError(err => {
        return throwError(err);
      }));
  }

  updateEvent(eventId: string, request: CreateEventRequestModel): Observable<HttpResponse<object>> {
    const url = '/api/v1/events/' + eventId;
    const jsonHeaders = new HttpHeaders().append('Content-Type', 'application/json');

    return this
      .http
      .put(
        url,
        request,
        {
          headers: jsonHeaders,
          observe: 'response',
          withCredentials: true
        }
      )
      .pipe(catchError(err => {
        return throwError(err);
      }));
  }

  deleteEvent(eventId: number): Observable<HttpResponse<object>> {
    const url = '/api/v1/events/' + eventId;

    return this
      .http
      .delete(
        url,
        {
          observe: 'response',
          withCredentials: true
        }
      )
      .pipe(catchError(err => {
        return throwError(err);
      }));
  }

  createReminder(eventId: string, request: CreateEventReminderRequestModel) {
    const url = '/api/v1/events/' + eventId + '/reminders';
    const jsonHeaders = new HttpHeaders().append('Content-Type', 'application/json');

    return this
      .http
      .post(
        url,
        request,
        {
          headers: jsonHeaders,
          observe: 'response',
          withCredentials: true
        }
      )
      .pipe(catchError(err => {
        return throwError(err);
      }));
  }

  deleteReminder(eventId: string, reminderId: string) {
    const url = '/api/v1/events/' + eventId + '/reminders/' + reminderId;

    return this
      .http
      .delete(
        url,
        {
          observe: 'response',
          withCredentials: true
        }
      )
      .pipe(catchError(err => {
        return throwError(err);
      }));
  }

  listEventsWithGraphQL(searchTextValue: string, pageContext: PageContextModel):
    Observable<HttpResponse<GraphqlListEventsModel>> {

    const url = '/graphql';
    const jsonHeaders = new HttpHeaders().append('Content-Type', 'application/json');

    const graphQlRequest =
`
query eventsByUserNoReminders($searchText: String, $page: Int!, $size: Int!) {
  eventsByUser(searchText: $searchText, page: $page, size: $size) {
    id
    beginAt
    occursEvery
    eventTime
    personInfo
    note
    useReminder
  }
}`;

    const request = {
      query: graphQlRequest,
      variables: {
        page: pageContext.page.toString(),
        size: pageContext.size.toString(),
        searchText: searchTextValue
      }
    };

    return this
      .http
      .post<GraphqlListEventsModel>(
        url,
        request,
        {
          headers: jsonHeaders,
          observe: 'response',
          withCredentials: true
        }
      )
      .pipe(catchError(err => {
        return throwError(err);
      }));
  }

  loadEventWithGraphql(eventIdValue: number): Observable<HttpResponse<GraphqlEventByIdModel>> {
    const url = '/graphql';
    const jsonHeaders = new HttpHeaders().append('Content-Type', 'application/json');

    const graphQlRequest =
`
query eventsByIdWithReminders($eventId: Int!) {
  eventsById(id: $eventId) {
    id
    beginAt
    occursEvery
    eventTime
    personInfo
    note
    useReminder
    reminders {
      id
      eventTime
      occursBefore
    }
  }
}`;

    const request = {
      query: graphQlRequest,
      variables: {
        eventId: eventIdValue
      }
    };

    return this
      .http
      .post<GraphqlEventByIdModel>(
        url,
        request,
        {
          headers: jsonHeaders,
          observe: 'response',
          withCredentials: true
        }
      )
      .pipe(catchError(err => {
        return throwError(err);
      }));

  }
}
