import { FlatpickrOptions } from 'ng2-flatpickr';
import { DateTimeUtils } from '../../../../../shared/utils/date-time.utils';

export class FlatpickrToolsModel {

  public static defaultTimeOptions: FlatpickrOptions = {
    enableTime: true,
    noCalendar: true,
    time_24hr: true,
    utc: true,
    defaultDate: '09:00',
    dateFormat: 'H:i:S'
  };

  public static defaultDateTimeOptions: FlatpickrOptions = {
    enableTime: false,
    utc: true,
    defaultDate: new Date(2019, 0, 1, 9, 0, 0, 0),
    dateFormat: 'Y-m-d'
  };

  public static parseTime(value): string {

    if (value instanceof Array) {
      return DateTimeUtils.formatTime(value[0]);
    } else {
      return value;
    }
  }

  public static parseDateTime(value): string {
    if (value instanceof Array) {
      return DateTimeUtils.formatDate(value[0]);
    } else {
      return value;
    }
  }
}
