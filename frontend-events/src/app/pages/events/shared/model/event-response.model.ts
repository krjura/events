export class EventResponseModel {
  id: number;
  beginAt: string;
  occursEvery: string;
  eventTime: string;
  nextOccurrence: string;
  personInfo: string;
  note: string;
  useReminder: boolean;
}
