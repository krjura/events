export class CreateEventReminderRequestModel {

  constructor(public eventTime: string, public occursBefore: string) {

  }
}
