import { EventResponseModel } from './event-response.model';

export class ListEventResponseModel {
  events: EventResponseModel[];
}
