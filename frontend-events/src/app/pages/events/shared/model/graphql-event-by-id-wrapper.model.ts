import { GraphqlEventModel } from './graphql-event.model';

export class GraphqlEventByIdWrapperModel {
  eventsById: GraphqlEventModel;
}
