import { GraphqlEventReminderModel } from './graphql-event-reminder.model';

export class GraphqlEventModel {
  id: number;
  beginAt: string;
  occursEvery: string;
  eventTime: string;
  personInfo: string;
  note: string;
  useReminder: boolean;
  reminders: GraphqlEventReminderModel[];
}
