export class GraphqlEventReminderModel {
  id: number;
  eventTime: string;
  occursBefore: string;
}
