import { GraphqlEventModel } from './graphql-event.model';

export class GraphqlEventsByUserWrapperModel {
  eventsByUser: GraphqlEventModel[];
}
