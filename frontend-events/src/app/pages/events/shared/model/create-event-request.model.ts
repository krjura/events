export interface CreateEventRequestModel {
  beginAt: string;
  occursEvery: string;
  eventTime: string;
  personInfo: string;
  note: string;
  useReminder: boolean;
}
