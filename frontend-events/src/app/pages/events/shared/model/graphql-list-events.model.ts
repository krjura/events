import { GraphqlEventsByUserWrapperModel } from './graphql-events-by-user-wrapper.model';

export class GraphqlListEventsModel {
  data: GraphqlEventsByUserWrapperModel;
}
