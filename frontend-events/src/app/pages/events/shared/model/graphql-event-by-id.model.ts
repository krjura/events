import { GraphqlEventByIdWrapperModel } from './graphql-event-by-id-wrapper.model';

export class GraphqlEventByIdModel {
  data: GraphqlEventByIdWrapperModel;
}
