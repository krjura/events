import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-error',
  templateUrl: './login-error.component.html',
  styleUrls: ['./login-error.component.scss']
})
export class LoginErrorComponent implements OnInit {

  constructor(private router: Router) {

  }

  ngOnInit() {
  }

  goToLogin() {
    const current = window.location;
    window.location.href = current.protocol + '//' + current.hostname + ':' + current.port + '/oauth2/authorization/google';
  }
}
