import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '@shared/services/authentication.service';

@Component({
  selector: 'app-auth-error',
  templateUrl: './auth-error.component.html',
  styleUrls: ['./auth-error.component.scss']
})
export class AuthErrorComponent implements OnInit {

  lastAuthErrorUrl: string = null;

  constructor(private authenticationService: AuthenticationService) {

  }

  ngOnInit() {
    this.lastAuthErrorUrl = this.authenticationService.getLastAuthErrorUrl();
  }

}
