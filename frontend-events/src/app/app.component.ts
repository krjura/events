import { Component } from '@angular/core';

import flatpickr from "flatpickr";

import { ToasterConfig } from 'angular2-toaster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend-events';

  public toasterConfig: ToasterConfig =
    new ToasterConfig({
      newestOnTop: true,
      showCloseButton: true,
      tapToDismiss: true,
      timeout: 2000,
      positionClass: 'toast-top-right'

    });

  // do not remove this. If you do tree shaking algorithm will remove flatpickr in its production build
  flatpickr = flatpickr("#non-existant-flatpickr-element");
}
